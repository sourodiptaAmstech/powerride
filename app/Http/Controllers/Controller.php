<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Validator;
use App\Model\Setting\Setting;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    protected function requestValidation($request,$rule){
        $validator =  Validator::make($request,$rule);
        if ($validator->fails()) {
            $array=$validator->getMessageBag()->toArray();
            foreach($array as $key=>$val){
                return (object)['message'=>$val[0],"field"=>$key,"status"=>"false"];
            }
        }
        return (object)['message'=>null,"field"=>null,"status"=>"true"];;

    }
    protected function checkNull($field){
        if($field===null)
        return "";
        else
        return $field;
    }

    public static function setting()
    {
        $site_settings = Setting::where('key', 'like', "%site%")->get();
        return $site_settings;
    }

    protected function convertToUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=-$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
        return $date;
    }
    protected function convertFromUTC($date,$timeZone){
        $timeZoneArray=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        $time="";
        if(is_array($timeZoneArray)){
            if($timeZoneArray[0]!==""){
                if((int)$timeZoneArray[0]>0){
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." +".$timeZoneArray[1]." minutes";
                    }
                }
                else{
                    $time=$timeZoneArray[0]." hour";
                    if(array_key_exists(1,$timeZoneArray)){
                        $time=$time." -".$timeZoneArray[1]." minutes";
                    }
                }
            }
            if($time!==""){
                $date=date("Y-m-d H:i",strtotime($time,strtotime($date)));
            }
        }
     //   echo $timeZone;
      //  echo $date; exit;
        return $date;
    }
    protected function curl($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $return = curl_exec($ch);
        curl_close ($ch);
        return $return;
    }

}
