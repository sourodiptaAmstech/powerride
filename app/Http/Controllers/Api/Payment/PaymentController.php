<?php
namespace App\Http\Controllers\Api\Payment;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\PassengersProfileService;
use App\Services\UsersDevices;
use App\Services\PaymentService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\ServiceRequestService;
use App\Services\EstimatedFareService;
use App\Services\TransactionLogService;
use App\Services\UserTrasactionEmailServices;
use App\Services\PayStackServices;
use Illuminate\Support\Facades\DB;
use Validator;


class PaymentController extends Controller
{
    public function verifyEmail(Request $request,$param)
    {
        $ute_id = base64_decode($param);
        $request->ute_id=$ute_id;
        $UserTrasactionEmailServices=new UserTrasactionEmailServices();
        $result=$UserTrasactionEmailServices->accessVerifyTrasactionEmail($request);
        return view('verify-email');
    }

    public function verifyEmailID(Request $request){
        try {
            $request->ute_id = $request->ute_id;
            $UserTrasactionEmailServices=new UserTrasactionEmailServices();
            $result=$UserTrasactionEmailServices->accessVerifyTrasactionEmail($request);

            return response(['message'=>"Email ID verified successfully.","data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }

    }

    public function addCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'reference'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // verify the payament by payment refernce_id

            $PayStackServices=new PayStackServices();
            $verifyArray=$PayStackServices->verifyTranscationByRefrence($request);
            $PaymentService=new PaymentService();
            if($verifyArray['statusCode']==200){
                $cardDetails=[
                    "email"=>$verifyArray['data']['data']['customer']["email"],
                    "customer_code"=>$verifyArray['data']['data']['customer']["customer_code"],
                    "authorization_code"=>$verifyArray['data']['data']['authorization']["authorization_code"],
                    "last_four"=>$verifyArray['data']['data']['authorization']["last4"],
                    "user_id"=>$request->user_id,
                    "signature"=>$verifyArray['data']['data']['authorization']["signature"],
                    "brand"=>trim($verifyArray['data']['data']['authorization']["card_type"]),
                    "channel"=>$verifyArray['data']['data']['authorization']["channel"],
                    "is_default"=>0,
                    "country_code"=>$verifyArray['data']['data']['authorization']["country_code"],
                    "status"=>$verifyArray['data']['data']['status']
                ];
                // Add card
              
                $addCards=$PaymentService->accessAddCards((object)$cardDetails);
               // dd( $addCards); exit;
                // Add transcation data
            }
            $card=$PaymentService->accessListCard($request);
            return response(['message'=>"Card Added.","data"=>["card"=>$card['data']],"errors"=>$card['errors']],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function ListCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // check for the verified payment email
            $UserTrasactionEmailServices=new UserTrasactionEmailServices();
            $stauts=$UserTrasactionEmailServices->checkVerifyEmail($request);
            $emailStatus="EMAILNOTADDED";
            $message="";$email="";
            if($stauts['statusCode']==403){
                $emailStatus="EMAILNOTVERIFIED";
                $message="Please verify your email id.";
                $email=$stauts['data'];


            }
            elseif($stauts['statusCode']==404){
                $emailStatus="EMAILNOTADDED";
                $message="Please add your email id.";
            }
            elseif($stauts['statusCode']==200){
                $emailStatus="EMAILVERIFIED";
                $email=$stauts['data'];

            }
            $PaymentService=new PaymentService();

            $card=$PaymentService->accessListCard($request);
            if(  $emailStatus=="EMAILVERIFIED")
                $message=$card['message'];

            return response(['message'=>$message,"data"=>["card"=>$card['data'],"emailStatus"=>$emailStatus,"email"=>$email],"errors"=>$card['errors']],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function DeleteCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'user_cards_id' => 'required|exists:user_cards,user_cards_id,user_id,'.Auth::user()->id
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,
                "field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],
                "e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $PaymentService=new PaymentService();
            $card=$PaymentService->accessDeleteCard($request);

            return response(['message'=>"Deleted Card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }


    }

    public function setDefaultCard(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required','user_cards_id' => 'required'];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $PaymentService=new PaymentService();
            $card=$PaymentService->accessSetDefaultCard($request);

            return response(['message'=>"Default card updated","data"=>[],"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }
    public function getCustomerTransaction(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByCustomerPagewise($request);
            foreach($returnRequest["data"] as $key=>$val){
                $returnRequest["data"][$key]['created_at']=$this->checkNull($returnRequest["data"][$key]['created_at']);
                if($returnRequest["data"][$key]['created_at']!=""){
                    $returnRequest["data"][$key]['created_at']=$this->convertFromUTC($returnRequest["data"][$key]['created_at'],$request->timeZone);
                }
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                // getPaidCancelAmount
                //accessGetCardDetailsByCardId

                $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
               //print_r($cancelAmount); exit;

                $returnRequest["data"][$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $returnRequest["data"][$key]['transaction']->cancelFee=(float)$cancelAmount['data'];
                $returnRequest["data"][$key]['transaction']->cost=(float)$returnRequest["data"][$key]['transaction']->cost-(float)$returnRequest["data"][$key]['transaction']->promo_code_value;//-(float)$cancelAmount;
                if($returnRequest["data"][$key]['transaction']->cost<=0){
                    $returnRequest["data"][$key]['transaction']->cost=0.00;
                }
                $returnRequest["data"][$key]['transaction']->cost= number_format((float)($returnRequest["data"][$key]['transaction']->cost), 2, '.', '');
                $returnRequest["data"][$key]['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                    $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    $returnRequest["data"][$key]['cardDetais']=$cardDetails['data'];
                }
            }
            return response(['message'=>"Default card updated","data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }


    public function currentWeekCalculation($request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            /**  get
             *   total completed trip
             *   total ammount paid to driver for all completed trip
             *   total tax collected for the completed trip
             *   total insurances fee collected for the trip
             *   total commission fee collected for the completed trip
             *
             *
             */
            $completedRide=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED"  and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            /**
             *  total cancel ammout collected by the driver
             *
             *
             */
            $totolCancelAmmoutPassenger=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  from transaction_log tl
            left join service_requests sr on sr.request_id=tl.payment_gateway_transaction_id and sr.request_status="COMPLETED" and sr.payment_status="COMPLETED"
            where sr.driver_id=? and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and tl.transaction_type="CANCELBYPASSENGERCHARGE" and tl.status="COMPLETED"',[ $request->user_id]);








              /**
             *  total amount for cancel fee due to driver cancel
             *
             *
             */

            $totolCancelAmmoutDriver=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.transaction_type="CANCELBYDRIVERCHARGE" and tl.status="PENDING"
            where sr.payment_status="PENDING" and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="CANCELBYDRIVER" and sr.driver_id=?',[ $request->user_id]);

            /***
             *
             *  total cash collected by the driver
             *
             */
            $completedRideByCash=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.payment_method="CASH" and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and  WEEKOFYEAR(sr.updated_at)=WEEKOFYEAR(NOW()) and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            $totalCashCardCollection=(float)$completedRide[0]->total;
            $totalTax =(float)$completedRide[0]->tax;
            $totalCommission=(float)$completedRide[0]->commission;
           // $totalTripecount =(float)$completedRide[0]->tripecount;
            $totalInsurance=(float)$completedRide[0]->insurance;
           // $totalPassengerCancelTrip=(float)$totolCancelAmmoutPassenger[0]->tripecount;
            $totalPassengerCancelAmount=(float)$totolCancelAmmoutPassenger[0]->total;
           // $totalDriverCancelTrip=(float)$totolCancelAmmoutDriver[0]->tripecount;
            $totalDriverCancelAmount=(float)$totolCancelAmmoutDriver[0]->total;
            $totalCashCollection=(float)$completedRideByCash[0]->total;
            $totalAdminTransfer=0;
            $totalPayOut=(float)$totalCashCollection+(float)$totalAdminTransfer;
           // $totalBalance=(float)$totalCashCardCollection-(float)$totalTax - (float)$totalCommission-(float)$totalInsurance-(float)$totalPayOut-(float)$totalDriverCancelAmount;
            $totalEarning= $totalCashCardCollection-$totalTax-$totalCommission-$totalInsurance-$totalPassengerCancelAmount-$totalDriverCancelAmount;



          /*  $Background=[
                "totalCashCardCollection"=>number_format((float)$totalCashCardCollection, 2, '.', ''),
                "totalTax"=>number_format((float)$totalTax, 2, '.', ''),
                "totalCommission"=>number_format((float)$totalCommission, 2, '.', ''),
                "totalTripecount"=>(int)number_format((float)$totalTripecount, 2, '.', ''),
                "totalInsurance"=>number_format((float)$totalInsurance, 2, '.', ''),
                "totalPassengerCancelTripCount"=>(int)number_format((float)$totalPassengerCancelTrip, 2, '.', ''),
                "totalPassengerCancelAmount"=>number_format((float)$totalPassengerCancelAmount, 2, '.', ''),
                "totalDriverCancelTripCount"=>(int)number_format((float)$totalDriverCancelTrip, 2, '.', ''),
                "totalDriverCancelAmount"=>number_format((float)$totalDriverCancelAmount, 2, '.', ''),
                "totalCashCollection"=>number_format((float)$totalCashCollection, 2, '.', ''),
                "totalPayOut"=>number_format((float)$totalPayOut, 2, '.', ''),
                "totalBalance"=>number_format((float)$totalBalance, 2, '.', ''),
                "totalAdminTransfer"=>number_format((float)$totalAdminTransfer, 2, '.', ''),
                "currency"=>"₦"
            ]; */
            return number_format((float)$totalEarning, 2, '.', '') ;
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function getDriverPaymentStatactics(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            /**  get
             *   total completed trip
             *   total ammount paid to driver for all completed trip
             *   total tax collected for the completed trip
             *   total insurances fee collected for the trip
             *   total commission fee collected for the completed trip
             *
             *
             */
            $completedRide=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            /**
             *  total cancel ammout collected by the driver
             *
             *
             */
            $totolCancelAmmoutPassenger=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total  from transaction_log tl
            left join service_requests sr on sr.request_id=tl.payment_gateway_transaction_id and sr.request_status="COMPLETED" and sr.payment_status="COMPLETED"
            where sr.driver_id=? and tl.transaction_type="CANCELBYPASSENGERCHARGE" and tl.status="COMPLETED"',[ $request->user_id]);








              /**
             *  total amount for cancel fee due to driver cancel
             *
             *
             */

            $totolCancelAmmoutDriver=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total
            FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.transaction_type="CANCELBYDRIVERCHARGE" and tl.status="PENDING"
            where sr.payment_status="PENDING" and sr.request_status="CANCELBYDRIVER" and sr.driver_id=?',[ $request->user_id]);

            /***
             *
             *  total cash collected by the driver
             *
             */
            $completedRideByCash=DB::select('SELECT count(sr.request_id) as tripecount, sum(tl.cost) as total,
            sum(tl.ride_insurance) as insurance, sum(tl.tax) as tax, sum(tl.commission) as commission    FROM service_requests sr
            left join transaction_log AS tl ON tl.request_id=sr.request_id and tl.payment_method="CASH" and tl.status="COMPLETED"
            where sr.payment_status="COMPLETED" and sr.request_status="COMPLETED" and sr.driver_id=?',[ $request->user_id]);

            $totalCashCardCollection=(float)$completedRide[0]->total;
            $totalTax =(float)$completedRide[0]->tax;
            $totalCommission=(float)$completedRide[0]->commission;
            $totalTripecount =(float)$completedRide[0]->tripecount;
            $totalInsurance=(float)$completedRide[0]->insurance;
            $totalPassengerCancelTrip=(float)$totolCancelAmmoutPassenger[0]->tripecount;
            $totalPassengerCancelAmount=(float)$totolCancelAmmoutPassenger[0]->total;
            $totalDriverCancelTrip=(float)$totolCancelAmmoutDriver[0]->tripecount;
            $totalDriverCancelAmount=(float)$totolCancelAmmoutDriver[0]->total;
            $totalCashCollection=(float)$completedRideByCash[0]->total;
            $totalAdminTransfer=0;
            $totalPayOut=(float)$totalCashCollection+(float)$totalAdminTransfer;
            $totalBalance=(float)$totalCashCardCollection-(float)$totalTax - (float)$totalCommission-(float)$totalInsurance-(float)$totalPayOut-(float)$totalDriverCancelAmount;



            $Background=[
                "totalCashCardCollection"=>number_format((float)$totalCashCardCollection, 2, '.', ''),
                "totalTax"=>number_format((float)$totalTax, 2, '.', ''),
                "totalCommission"=>number_format((float)$totalCommission, 2, '.', ''),
                "totalTripecount"=>(int)number_format((float)$totalTripecount, 2, '.', ''),
                "totalInsurance"=>number_format((float)$totalInsurance, 2, '.', ''),
                "totalPassengerCancelTripCount"=>(int)number_format((float)$totalPassengerCancelTrip, 2, '.', ''),
                "totalPassengerCancelAmount"=>number_format((float)$totalPassengerCancelAmount, 2, '.', ''),
                "totalDriverCancelTripCount"=>(int)number_format((float)$totalDriverCancelTrip, 2, '.', ''),
                "totalDriverCancelAmount"=>number_format((float)$totalDriverCancelAmount, 2, '.', ''),
                "totalCashCollection"=>number_format((float)$totalCashCollection, 2, '.', ''),
                "totalPayOut"=>number_format((float)$totalPayOut, 2, '.', ''),
                "totalBalance"=>number_format((float)$totalBalance, 2, '.', ''),
                "totalAdminTransfer"=>number_format((float)$totalAdminTransfer, 2, '.', ''),
                "currentWeek"=>$this->currentWeekCalculation($request),
                "currency"=>"₦"
            ];
            return response(['message'=>"Driver Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getDriverTransaction(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[ 'timeZone'=>'required'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],
                "errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByDriverPagewise($request);
            $driverTransactionDetails=[];
            //print_r($returnRequest); exit;
            foreach($returnRequest["data"] as $key=>$val){
                $driverTransactionDetailsTemp=[];
                $driverTransactionDetailsTemp['transactionCreated']="";
                if($this->checkNull($returnRequest["data"][$key]['transactionCreatedTime'])!=""){
                    $driverTransactionDetailsTemp['transactionCreated']=$this->convertFromUTC($returnRequest["data"][$key]['transactionCreatedTime'],$request->timeZone);
                }
                $driverTransactionDetailsTemp['request_id']=$returnRequest["data"][$key]['request_id'];
                $driverTransactionDetailsTemp['request_no']=$returnRequest["data"][$key]['request_no'];
                $driverTransactionDetailsTemp['passenger_id']=$returnRequest["data"][$key]['passenger_id'];
                $driverTransactionDetailsTemp['driver_id']=$returnRequest["data"][$key]['driver_id'];
                $driverTransactionDetailsTemp['request_type']=$returnRequest["data"][$key]['request_type'];
                $driverTransactionDetailsTemp['request_status']=$returnRequest["data"][$key]['request_status'];
                $driverTransactionDetailsTemp['isFemaleFriendly']=$returnRequest["data"][$key]['isFemaleFriendly'];
                $driverTransactionDetailsTemp['transaction_log_id']=$returnRequest["data"][$key]['transaction_log_id'];
                $driverTransactionDetailsTemp['ride_insurance']=$returnRequest["data"][$key]['ride_insurance'];
                $driverTransactionDetailsTemp['tax']=$returnRequest["data"][$key]['tax'];
                $driverTransactionDetailsTemp['cost']=$returnRequest["data"][$key]['cost'];
                $driverTransactionDetailsTemp['payment_method']=$returnRequest["data"][$key]['payment_method'];
                $driverTransactionDetailsTemp['types']=$returnRequest["data"][$key]['types'];
                $driverTransactionDetailsTemp['commission']=$returnRequest["data"][$key]['commission'];
                $driverTransactionDetailsTemp['transaction_type']=$returnRequest["data"][$key]['transaction_type'];
                $driverTransactionDetailsTemp['is_paid']=$returnRequest["data"][$key]['is_paid'];
                $driverTransactionDetailsTemp['status']=$returnRequest["data"][$key]['status'];
                $driverTransactionDetailsTemp['cancelAmount']=0;
                if( $driverTransactionDetailsTemp['request_status']!="CANCELBYDRIVER"){
                    $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                    $driverTransactionDetailsTemp['cancelAmount']=$cancelAmount['data'];
                    $driverTransactionDetailsTemp['cost']=(float)$driverTransactionDetailsTemp['cost']-(float)$driverTransactionDetailsTemp['cancelAmount']-(float)$driverTransactionDetailsTemp['commission']-(float)$driverTransactionDetailsTemp['tax']-(float)$driverTransactionDetailsTemp['ride_insurance'];
                }
                if( $driverTransactionDetailsTemp['cost']<=0){
                    $ $driverTransactionDetailsTemp['cost']=0.00;
                }
                $driverTransactionDetailsTemp['cost']= number_format((float)( $driverTransactionDetailsTemp['cost']), 2, '.', '');
               // $driverTransactionDetailsTemp['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                   // $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    //$driverTransactionDetailsTemp['cardDetais']=$cardDetails['data'];
                }
                $driverTransactionDetails[]=$driverTransactionDetailsTemp;
            }
            $returnRequest['data']=$driverTransactionDetails;
            return response(['message'=>"List","data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
    }

}
