<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use App\Services\NotificationService;

use Validator;

class NotificationController extends Controller
{
    public function getNotification(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");

            $rule=[
                'timeZone'=>'required',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422);
            };
            $request->user_id=Auth::user()->id;
            $NotificationService=new NotificationService();
            $mes=$NotificationService->accessGetNotification($request->user_id,$request['timeZone']);







            return response(['message'=>'notification list',"data"=>$mes,"errors"=>[]],(int)200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
