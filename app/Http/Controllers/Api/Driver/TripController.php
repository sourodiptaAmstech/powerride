<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Services\EstimatedFareService;
use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\AuthService;
use App\Services\DriversServiceType;
use App\Services\DriverPreferencesService;
use App\Services\ServiceRequestService;
use App\Services\UnitConvertionService;
use App\Services\RequestServices;
use App\Services\LocationService;
use App\Services\TransactionLogService;
use App\Http\Controllers\Api\Background\DriversBackgroundController;
use App\Services\PaymentService;
use Validator;
use Exception;
use PHPUnit\Util\Printer;
use App\Services\PassengersProfileService;
use App\Services\NotificationServices;
use App\Services\SettingServices;
use App\Services\NotificationService;

class TripController extends Controller
{
    public function reject(Request $request){
        try{

            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();

            $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ACTIVE","driver_id"=>$request->user_id],"REQUESTED");
            $RequestServices=new RequestServices();
            $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"DECLINE","driver_id"=>$request->user_id],"REQUESTED");
            return response(['message'=>trans("api.SYSTEM_MESSAGE.REQUEST_DECLINE"),"data"=>(object)[],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    private function calculateFinalPayable($data){
        $EstimatedFareService=new EstimatedFareService();
        return $EstimatedFareService->accessCalculateFinalPayable((object)$data);
    }
    public function tripControl(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'request_id' => "required",
                'timeZone'=>'required',
                'tripStatus'=>'required|in:ACCEPTED,REACHED,STARTED,DROP,PAYMENT,RATING,COMPLETED',
                 'payment_method'=>'required_if:tripStatus,PAYMENT|in:CARD,CASH',
                 'rating'=>'required_if:tripStatus,RATING',
                 'comment'=>'required_if:tripStatus,RATING'

            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            $DriversProfileService=new DriversProfileService();
            $RequestServices=new RequestServices();
            $ServiceRequestService=new ServiceRequestService();
            $LocationService = new LocationService();
            $NotificationServices =new NotificationServices();
            $message="";
            $status="";
            $nextStep="";
            switch($request->tripStatus){
                case "ACCEPTED":
                    //updating the driver profile
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ONRIDE","driver_id"=>$request->user_id],"requested");
                    // updating the service request log
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$request->request_id,"status"=>"ONRIDE","driver_id"=>$request->user_id],"REQUESTED");
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"ACCEPTED","driver_id"=>$request->user_id]);
                    $message=trans("api.SYSTEM_MESSAGE.REQUEST_ACCEPTED");
                    $status="ACCEPTED";
                    $nextStep="REACHED";
                      // push
                      $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_ACCEPTED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_ACCEPTED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_ACCEPTED"),
                        "type"=>"RIDEACCEPTED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                        $Notification=new NotificationService();
                        $Notification->accessCreateNotification((object)$pushDate);

                break;
                case "REACHED":
                    // updating the service request
                    $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"REACHED","driver_id"=>$request->user_id]);
                    $LocationServiceReturn= $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                    $message=trans("api.SYSTEM_MESSAGE.WAIT_FOR_CUSTOMER");
                    $status="REACHED";
                    $nextStep="STARTED";
                     // push
                     $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_REACHED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REACHED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REACHED"),
                        "type"=>"RIDEREACHED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        // send sms
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        // email
                        $Notification=new NotificationService();
                        $Notification->accessCreateNotification((object)$pushDate);
                break;
                case "STARTED":
                      //caculate waiting time;
                      $ServiceRequestServiceReturn= $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"STARTED","driver_id"=>$request->user_id]);
                      $LocationServiceReturn= $LocationService->accessUpdateLocationStartedTime((object)["request_id"=>$request->request_id,"types"=>"source"]);
                      $message=trans("api.SYSTEM_MESSAGE.PROCEED_TO_DESTINATION");//"Please proceed towards the destination.";
                      $status="STARTED";
                      $nextStep="DROP";
                break;
                case "DROP":
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,"request_status"=>"DROP","driver_id"=>$request->user_id]);
                    $LocationServiceReturn = $LocationService->accessUpdateLocationReachedTime((object)["request_id"=>$request->request_id,"types"=>"destination"]);
                    $ServiceRequestServiceReturn = $ServiceRequestService->accessGetRequestByID((object)['request_id'=>$request->request_id]);
                    //   print_r( $ServiceRequestServiceReturn ); exit;
                    if($ServiceRequestServiceReturn->payment_method=="CARD"){
                        $requestedRide=[
                            "request_no"=>$ServiceRequestServiceReturn->request_no,
                            "request_id"=>$ServiceRequestServiceReturn->request_id,
                            "request_type"=>$ServiceRequestServiceReturn->request_type,
                            "request_status"=>$ServiceRequestServiceReturn->request_status,
                            "payment_method"=>$ServiceRequestServiceReturn->payment_method,
                            "staredFromSource_on"=>$ServiceRequestServiceReturn->started_from_source,
                            "dropped_on_destination"=>$ServiceRequestServiceReturn->dropped_on_destination,
                            "driver_rating_status"=>$ServiceRequestServiceReturn->driver_rating_status,
                            "user_cards_id"=>$ServiceRequestServiceReturn->card_id,
                            "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        ];
                        $reqRq = $this->calculateFinalPayable($requestedRide);
                        if((float)($reqRq['cost'])-(float)($reqRq['promo_code_value'])>0){
                            $PaymentService = new PaymentService();
                            $retunPayment=$PaymentService->accesssMakeCardPayment((object)$reqRq,(object)$requestedRide);
                            //print_r($retunPayment); exit;
                            if($retunPayment['statusCode']==200){
                                // if payment is success update the transcation table
                                $TransactionLogService = new TransactionLogService();
                                $reqRq['payment_gateway_charge']=$retunPayment['data']['fees'];
                                $reqRq['payment_gateway_transaction_id']=$retunPayment['data']['reference'];
                                $TransactionReturn= $TransactionLogService->updateTransactionDetails((object)$reqRq);
                                if($TransactionReturn['statusCode']===200){
                                    $TransactionLogService->makeCancelPayment((object)$requestedRide);
                                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                    "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                    $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");
                                    $status="PAYMENT";
                                    $nextStep="RATING";
                                }
                            }
                            else{
                                // handle paymanet failed
                            }
                        }
                        else{
                             // if payment is success update the transcation table
                             $TransactionLogService = new TransactionLogService();
                             $reqRq['payment_gateway_charge']=0;
                             $reqRq['payment_gateway_transaction_id']=$reqRq['promo_code'];
                             $TransactionReturn=  $TransactionLogService->updateTransactionDetails((object)$reqRq);
                             if($TransactionReturn['statusCode']===200){
                                $TransactionLogService->makeCancelPayment((object)$requestedRide);
                                 $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                                 "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                                 $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");//"Thank you for collecting the payment";
                                 $status="PAYMENT";
                                 $nextStep="RATING";
                             }
                        }
                    }
                    else{
                        $message=trans("api.SYSTEM_MESSAGE.CALCULATED_PAYABLE");
                        $status="DROP";
                        $nextStep="PAYMENT";
                    }
                break;
                case "PAYMENT":
                    if($request->payment_method=="CASH"){
                      // update the trascation log table as payament completed,
                      // update the service request table to PAYMENT and mark payment completed,
                      $TransactionLogService = new TransactionLogService();
                      $TransactionLogService->updateCreditNoteStatusForRide((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                      $TransactionLogService->makeCancelPayment((object)["request_id"=>$request->request_id,"status"=>"COMPLETED"]);
                      $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                      "request_status"=>"PAYMENT","payment_method"=>"CASH","payment_status"=>"COMPLETED","driver_id"=>$request->user_id]);
                      $message=trans("api.SYSTEM_MESSAGE.PAYMENT_COLLECTED");
                      $status="PAYMENT";
                      $nextStep="RATING";
                    }
                   // else if()

                break;
                case "RATING":
                    // update the service request with rating comment

                    $ServiceRequestServiceReturn = $ServiceRequestService->accessUpdateDriver((object)["request_id"=>$request->request_id,
                    "request_status"=>"RATING","rating_by_driver"=>$request->rating,"comment_by_driver"=>$request->comment,
                    "driver_rating_status"=>"GIVEN","driver_id"=>$request->user_id]);
                    $message=trans("api.SYSTEM_MESSAGE.RATING_CUSTOMER");
                    $status="RATING";
                    $nextStep="COMPLETED";
                     // push
                     $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.RIDE_COMPLETED"),
                        "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED"),
                        "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED"),
                        "type"=>"RIDECOMPLETED",
                        "user_id"=>$ServiceRequestServiceReturn->passenger_id,
                        "setTo"=>'SINGLE',
                        "request_id"=>$ServiceRequestServiceReturn->request_id
                    ];
                    $NotificationServices->sendPushNotification((object)$pushDate);
                    // send sms
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    // email
                    $Notification=new NotificationService();
                    $Notification->accessCreateNotification((object)$pushDate);
                break;
                case "COMPLETED":
                    $message=trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED_DRIVER");
                    $status="COMPLETED";
                    $nextStep="NORMAL";
                break;
            }
            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function tripHistory(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                if($requestBooking["data"][$key]['request_status']=="CANCELBYDRIVER"){
                    $requestBooking["data"][$key]['started_from_source']=$requestBooking["data"][$key]['created_at'];
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking["data"][$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];

                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking["data"][$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function tripDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $PassengersProfileService=new PassengersProfileService();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByDriverId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking as $key=>$val){
                $requestBooking[$key]['schedule_datetime']=$this->checkNull($requestBooking[$key]['schedule_datetime']);
                if($requestBooking[$key]['schedule_datetime']!=""){
                    $requestBooking[$key]['schedule_datetime']=$this->convertFromUTC($requestBooking[$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking[$key]['started_from_source']=$this->checkNull($requestBooking[$key]['started_from_source']);
                if($requestBooking[$key]['started_from_source']!=""){
                    $requestBooking[$key]['started_from_source']=$this->convertFromUTC($requestBooking[$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking[$key]['dropped_on_destination']=$this->checkNull($requestBooking[$key]['dropped_on_destination']);
                if($requestBooking[$key]['dropped_on_destination']!=""){
                    $requestBooking[$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking[$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking[$key]['accepted_on']=$this->checkNull($requestBooking[$key]['accepted_on']);
                if($requestBooking[$key]['accepted_on']!=""){
                    $requestBooking[$key]['accepted_on']=$this->convertFromUTC($requestBooking[$key]['accepted_on'],$request->timeZone);
                }
                $requestBooking[$key]['created_at']=$this->checkNull($requestBooking[$key]['created_at']);
                if($requestBooking[$key]['created_at']!=""){
                    $requestBooking[$key]['created_at']=$this->convertFromUTC($requestBooking[$key]['created_at'],$request->timeZone);
                }
                if($requestBooking[$key]['request_status']=="CANCELBYDRIVER"){
                    $requestBooking[$key]['started_from_source']=$requestBooking[$key]['created_at'];
                }
                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking[$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking[$key]['request_id']]);
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];



                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }

                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');

                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking[$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];

                $ProfileData=$PassengersProfileService->accessGetProfile((object)['user_id'=>$requestBooking[$key]['passenger_id']]);

                $myBooking[$key]['ProfileData']=$ProfileData['data'];
            }
            $requestBooking= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking[0],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function cancelRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];


            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get request details,
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $DriverProfile=new DriversProfileService();
            $SettingServices=new SettingServices();
            $TransactionLogService=new TransactionLogService();
            $NotificationServices =new NotificationServices();

            $getRequestByID=$ServiceRequestService->accessGetRequestByID($request);
            //print_r($getSerReqLog); exit;
            if($getRequestByID->request_status=="ACCEPTED" || $getRequestByID->request_status=="REACHED")
            {
                $getRequestByID->request_status="CANCELBYDRIVER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="ONRIDE"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYDRIVER"],$status="ONRIDE");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="ONRIDE");
                    }
                }
                $cancelAmount=$SettingServices->getValueByKey("driver_cancellation_charge");
                $cancelationCharger=[
                "request_id"=>$request->request_id,
                'ride_insurance'=>0,
                'per_minute'=>0,
                'per_distance_km'=>0,
                'minimum_waiting_time_in_minutes'=>0,
                'waiting_charge_per_min'=>0,
                'waitTime'=>0,
                'duration_hr'=>0,
                'duration_min'=>0,
                'duration_sec'=>0,
                'duration'=>0,
                'distance_km'=>0,
                'distance_miles'=>0,
                'distance_meters'=>0,
                "cost"=>$cancelAmount,
                "payment_method"=>"NONE",
                "payment_gateway_charge"=>0,
                'types'=>'CREDIT',
                "transaction_type"=>"CANCELBYDRIVERCHARGE",
                "is_paid"=>"N",
                "status"=>"PENDING"
                ];
                $TransactionLogService->createCreditNoteForRide($cancelationCharger);
                $message=trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_DRIVER", [ 'cancelAmount' =>$cancelAmount, 'currency' => '₦' ]);//"Ride request cancelled by you. The cancellation charge of $cancelAmount ₦ will be deducted from your wallet.";
                $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                        "text"=>$message,
                        "body"=>$message,
                        "type"=>"CANCELBYDRIVER",
                        "user_id"=>$getRequestByID->driver_id,
                        "setTo"=>'SINGLE'
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER"),
                            "type"=>"CANCELBYDRIVER",
                            "user_id"=>$getRequestByID->passenger_id,
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
            }
            else{

                $message=trans("api.SYSTEM_MESSAGE.ON_RIDE");
            }
            return response(['message'=>$message,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

}
