<?php

namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use App\Model\Document\DriverDocuments;
use App\Model\ServiceType\DriverServiceType;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;

use Validator;

class DocumentController extends Controller
{
    public function uploadDocument(Request $request,$document_id){
        try{
            $id=Auth::user()->id;
            $rule=['file' => 'required|mimes:jpeg,jpg,bmp,png'];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){
                return response(['message'=>$validator->message,"field"=>$validator->field,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")])],422);
            }
            $Document = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->get()->toArray();
            if(!empty($Document)){
                $DriverDocuments = DriverDocuments::where('user_id', $id)->where('document_id', $document_id)->firstOrFail();
                if(isset($request->file) && !empty($request->file)){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments->url="http://104.131.120.166/powerride/public/".$picture;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }
            else{
                if(isset($request->file) && !empty($request->file)){
                    $picture = $request->file->store('public/driver/'.$id.'/documents');
                    $picture=str_replace("public", "storage", $picture);
                    $DriverDocuments=new DriverDocuments;
                    $DriverDocuments->url="http://104.131.120.166/powerride/public/".$picture;
                    $DriverDocuments->user_id=$id;
                    $DriverDocuments->document_id=$document_id;
                    $DriverDocuments->status='ASSESSING';
                    $DriverDocuments->save();
                }
            }

            return response(['message'=>trans("api.SYSTEM_MESSAGE.DOCUMENT_UPDATED"),"data"=>[],"errors"=>array("exception"=>["Document updated successfully."])],201);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e)
        {
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }

    }
    public function list(Request $request)
    {
        try{

            $id=Auth::user()->id;
            
            $model_type = DriverServiceType::where('user_id',$id)->select('model_is_tinted')->first();

            $VehicleDocuments = DB::table('documents')
            ->select('documents.*',
                        DB::raw('COALESCE((select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' .$id . '),"") AS driver_document_status'),
                        DB::raw('COALESCE((select driver_documents.url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_url'),
                        DB::raw('COALESCE((select driver_documents.expires_at  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_expires_at')
                    )
            ->where('type', 'VEHICLE')->where('status', 1)
            ->get();
            $DriverDocuments =  DB::table('documents')
            ->select('documents.*',
                        DB::raw('COALESCE((select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_status'),
                        DB::raw('COALESCE((select driver_documents.url  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_url'),
                        DB::raw('COALESCE((select driver_documents.expires_at  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' .$id . '),"") AS driver_document_expires_at')
                    )
                    ->where('type', 'DRIVER')->where('status', 1)
                    ->get();

                    $VehicleImage = DB::table('documents')
                    ->select('documents.*',
                        DB::raw('COALESCE((select driver_documents.status  from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_status'),
                        DB::raw('COALESCE((select driver_documents.url   from driver_documents where documents.id=driver_documents.document_id AND driver_documents.user_id=' . $id . '),"") AS driver_document_url')
                    )
                    ->where('type', 'VEHICLE IMAGE')->where('status', 1)
                    ->get();
                    //       $driver = Auth::user();
                    $Documnets=[
                        'VehicleDocuments' => $VehicleDocuments,
                        'DriverDocuments' => $DriverDocuments,
                        'VehicleImage' => $VehicleImage,
                        'model_is_tinted' => $model_type->model_is_tinted
                    ];

                    return response(['message'=>trans("api.SYSTEM_MESSAGE.DOCUMENT_LIST"),"data"=>(array)$Documnets,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
                }
                catch(\Illuminate\Database\QueryException  $e){
                    return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
                }
                catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
                    return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
                }
                catch(ModelNotFoundException $e)
                {
                    return response(['message'=>trans("api.notAuthorize"),"errors"=>array("exception"=>["Invalid credentials"])],401);
                }
            }


}

