<?php

namespace App\Http\Controllers\Api\TermCondition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SettingServices;
use DB;


class TermConditionController extends Controller
{
    public function getTermConditionPassenger(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $legal = DB::table('settings')->select('value')->where('key', 'legal')->first();

            $page_privacy = DB::table('settings')->select('value')->where('key', 'page_privacy')->first();

            $condition_privacy = DB::table('settings')->select('value')->where('key', 'condition_privacy')->first();

            return response()->json(['response' => 'success','legal'=>$legal->value,'privacy_policy'=>$page_privacy->value,'terms_conditions'=>$condition_privacy->value]);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getTermConditionDriver(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $legal = DB::table('settings')->select('value')->where('key', 'legal_driver')->first();

            $page_privacy = DB::table('settings')->select('value')->where('key', 'page_privacy_driver')->first();

            $condition_privacy = DB::table('settings')->select('value')->where('key', 'condition_privacy_driver')->first();

            return response()->json(['response' => 'success','legal'=>$legal->value,'privecy_policy'=>$page_privacy->value,'terms_conditions'=>$condition_privacy->value]);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
