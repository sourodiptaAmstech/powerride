<?php
namespace App\Http\Controllers\Api\Background;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Model\Device\UserDevices;
use App\Services\DriversProfileService;
use App\Services\ServiceRequestService;

use App\Services\RequestServices;
use App\Services\EstimatedFareService;
use App\Services\LocationService;
use App\Services\UserService;
use App\Services\UsersDevices;
use App\Services\DriversServiceType;
use App\Services\ServiceTypeMst;
use App\Services\NotificationServices;
use App\Services\NotificationService;
use App\Services\SettingServices;

use Validator;

class PassengerBackgroundController extends Controller
{
    function searchMultiArray($value, $array,$keys) {
        foreach ($array as $key => $val) {
            if ($val[$keys] === $value) {
                return $value;
            }
        }
        return null;
     }

     private function requestedRide($requestDeatils){

        $RequestServices=new RequestServices();
        $EstimatedFareService =new EstimatedFareService();
        $LocationService=new LocationService();
        $DriversProfileService=new DriversProfileService();
        $UserService=new UserService();
        $UsersDevices= new UsersDevices();
        $DriversServiceType=new DriversServiceType();
        $ServiceTypeMst= new ServiceTypeMst();
        $SettingServices=new SettingServices();

        // Get Driver Profile Details
        $driverProfileData=$DriversProfileService->accessGetProfile((object)["user_id"=>$requestDeatils['driver_id']]);
        // get driver login type

        $driverUserdetails=$UserService->accessGetUsers((object)["user_id"=>$requestDeatils['driver_id']]);

        $DriverProfile=$DriversProfileService->setProfileData($driverProfileData['data'],$driverUserdetails->login_type);

        // get driver service details

        $DriversServiceDetails=$DriversServiceType->accessGetService((object)["user_id"=>$requestDeatils['driver_id']]);

        // get ervice mst details

        $ServiceDetails=$ServiceTypeMst->accessGetNameByAllID((object)["service_type_id"=>$DriversServiceDetails['data']->service_type_id]);

        //get sos number
        $sosnumber=$SettingServices->getValueByKey("sos_number");


        // get drive live location

        $UsersDevicesReturn=$UsersDevices->accessGetDevice((object)['user_id'=>$requestDeatils['driver_id']]);

        // get travel loactions

        $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

         $locDetails=$LocationService->accessGetLocation($requestDeatils['request_id']);

         foreach($locDetails as $locKey =>$locVal){
             switch($locVal['types']){
                 case "source":
                     $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                 break;
                 case "destination":
                     $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                 break;
                 case "waypoint":
                     $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                 break;
             }
         }


         $requestData=[
            'driverProfile'=>$DriverProfile,
            "driverService"=>[
                "registration_no"=>$DriversServiceDetails['data']->registration_no,
                "model"=>$DriversServiceDetails['data']->model,
                "driver_service_type_id"=>$DriversServiceDetails['data']->driver_service_type_id,
                "model_year"=>(int)$DriversServiceDetails['data']->model_year,
                "make"=>$DriversServiceDetails['data']->make,
                "serviceName"=>$DriversServiceDetails['data']->serviceName
            ],
            "ServiceDetails"=>$ServiceDetails[0],
            "requestDetials"=>$requestDeatils,
            "driver_location"=>["latitude"=>(float)$UsersDevicesReturn->latitude,"longitude"=>(float)$UsersDevicesReturn->longitude],
            "location_details"=>(array)$locDetailsArray,
            "sos"=>$sosnumber,
        ];


        //






        return $requestData;
    }
    private function calculateFinalPayable($data){
        $EstimatedFareService=new EstimatedFareService();
        return $EstimatedFareService->accessCalculateFinalPayable((object)$data);
    }

    private function noResponseToDutyCall($data,$serviceLog){
        $isDutyCall="true";
        foreach($serviceLog as $key=>$val){
            if($val['status']=="REQUESTED"){
                $isDutyCall= (strtotime(date("Y-m-d H:i:s"))<strtotime("+60 seconds", strtotime($val['updated_at'])))?"true":"false";
                if($isDutyCall=="false"){
                    $DriversProfileService=new DriversProfileService();
                    $DriversProfileServiceReturn=$DriversProfileService->accessSetRequestToDriver((object)["driver_service_status"=>"ACTIVE","driver_id"=>$val['driver_id']],"REQUESTED");
                    $RequestServices=new RequestServices();
                    $RequestServicesReturn=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"status"=>"DECLINE","driver_id"=>$val['driver_id']],"REQUESTED");
                }
            }
        }
        return $isDutyCall;
    }

     public function get(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'device_latitude'=>'required',
                'device_longitude'=>'required'
            ];
            $status="NORMAL";
            $messsage="";
            $rideDetails=[];

            $request->user_scope="passenger-service";
            $request->user_id=Auth::user()->id;

          //  $UsersDevices=new UsersDevices();
            $ServiceRequestService=new ServiceRequestService();
            $requestServices=new RequestServices();
            $LocationService=new LocationService();

            //update lat log
            $UserDevices = UserDevices::where('user_id',$request->user_id)->first();
            $UserDevices->latitude = $request->device_latitude;
            $UserDevices->longitude = $request->device_longitude;
            $UserDevices->save();

            // get request details ;
            $requestDetails=$ServiceRequestService->accessGetRiderActiveRequests($request);

          // $requestDetails=[];
            foreach($requestDetails as $key=>$val){
                switch($val['request_status']){
                    case "RIDESEARCH":
                        $isSearch= (strtotime(date("Y-m-d H:i:s"))<strtotime("+6 minutes", strtotime($val['updated_at'])))?"true":"false";
                        $data=[
                            'request_id'=>$val['request_id'],
                            "service_type_id"=>$val['service_type_id'],
                            "locationDetails"=>$LocationService->accessGetLocation($val['request_id']),
                            "passenger_id"=>$request->user_id,
                            "request_type"=>$val["request_type"],
                            "driver_service_status"=>"REQUESTED",
                            "isFemaleFriendly"=>$val['isFemaleFriendly']
                        ];


                        //get data from the service request log
                        $serviceLog=$requestServices->accessGetSerReqLog((object)$data);

                        if(empty($serviceLog)){
                            // check if still service can be serach

                            if($isSearch=="false"){
                                $status="NOSERVICEFOUND";
                                $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                // update the service request table's request_status with noServiceFound

                                $data['request_status']=$status;
                                $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);

                            break;
                            }

                            /**
                             * system will first search the estimanted fare; as the estimated fare may change due to
                             * various factor like traffic, avalibility of the driver, as it posible that customer press the request button from the mobile app
                             * after 10 -15min or more;
                             */

                             /**** ESTIMATED FARE */

                             $EstimatedFareService =new EstimatedFareService();


                             $retunEsti=$EstimatedFareService->accessGetFareByServiceType((object)$data);
                             if($retunEsti["statusCode"]!=200){
                                 $status="NOSERVICEFOUND";
                                 $messsage="Sorry no driver is available at this moment. Please try after some time.";
                                break;
                            }



                            /**** End */

                            /*** Driver Search   */

                           // $driverDetails=$requestServices->accessGetDriverWithInRadius((object)$data,$serviceLog);

                            /*** Driver Not Found */
                            $isDriverFound="false";
                            if(array_key_exists("data",$retunEsti)){
                                if(array_key_exists(0,$retunEsti['data'])){
                                    if(array_key_exists('carDetails',$retunEsti['data'][0])){
                                        if(array_key_exists('statusCode',$retunEsti['data'][0]['carDetails'])){
                                            if($retunEsti['data'][0]['carDetails']['statusCode']==200){
                                                $isDriverFound="true";
                                            }
                                        }
                                    }
                                }
                            }

                            if($isDriverFound=="false"){
                                $status="NOSERVICEFOUND";
                                $messsage="Sorry no driver is available at this moment. Please try after some time.";
                                // update the service request table's request_status with noServiceFound
                                $data['request_status']=$status;
                                $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                break;
                            }

                            /*** End Driver Not Found */

                            /**
                             * Driver found!! Ya Ya.. :)
                             * Now insert the driver and request details in the service request log
                             * and update the service request with the estimated fare and update the driver profile service status as requested
                             * Now send the response as RideSearch
                             *
                             */

                             $data["driver_id"]=($retunEsti['data'][0]['carDetails']['data'][0])->user_id;
                             $data["estimated_fare"]=$retunEsti['data'][0]['estimated_fare'];
                             $data["estimated_fare"]["request_id"]=$data["request_id"];
                             $data["estimated_fare"]["service_type_id"]=$data["service_type_id"];
                          //
                             $DriversProfileService=new DriversProfileService();
                             $driverProfile=$DriversProfileService->accessSetRequestToDriver((object)$data);

                             if($driverProfile['statusCode']!=200){
                                 $status="RIDESEARCH";
                                 $messsage="Connecting with near by drivers.";
                                break;
                            }
                            else if($driverProfile['statusCode']==200){
                                // push
                                $pushDate=[
                                    "title"=>trans("api.NOTIFICATION.TITLE.RIDE_REQUEST"),
                                    "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REQUEST"),
                                    "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REQUEST"),
                                    "type"=>"RIDEREQUEST",
                                    "user_id"=>$data["driver_id"],
                                    "setTo"=>'SINGLE',
                                    "request_id"=>$data["request_id"]
                                ];
                                $NotificationServices =new NotificationServices();
                                $Notification=new NotificationService();
                                    $Notification->accessCreateNotification((object)$pushDate);
                                $NotificationServices->sendPushNotification((object)$pushDate);
                                // send sms
                               // $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                                // email
                            }


                            $reqSer=$requestServices->accessInsertSerReqLog((object)$data);
                            $data["estimatedFare"]=$retunEsti["data"][0];
                            $estDl= $EstimatedFareService->accessInsertEstimatedFare((object)$data['estimated_fare']);
                            $status="RIDESEARCH";
                            $messsage="Connecting with near by drivers.";

                            ############### END OF DRIVER SEARCH FOR FIRST EVER DRIVE ############################
                        }
                        else{

                            if($this->searchMultiArray("REQUESTED", $serviceLog,"status")=="REQUESTED"){
                                // if driver dont response to duty call
                                    $this->noResponseToDutyCall($data,$serviceLog);
                                    $status="RIDESEARCH";
                                    $messsage="Connecting with near by drivers.";
                            }
                            else if($this->searchMultiArray("DECLINE", $serviceLog,"status")=="DECLINE"){

                                if($isSearch=="false"){

                                    $status="NOSERVICEFOUND";
                                    $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                    // update the service request table's request_status with noServiceFound

                                    $data['request_status']=$status;
                                    $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                    break;
                                }
                                $EstimatedFareService =new EstimatedFareService();
                                /*** Driver Search   */
                                $driverDetails=$EstimatedFareService->accessGetDriverWithInRadius((object)$data,$serviceLog);

                                /*** Driver Not Found */
                                if($driverDetails['statusCode']!=200){
                                    $status="NOSERVICEFOUND";
                                    $messsage="Sorry no driver is available at this moment. Please try after some time.";

                                    // update the service request table's request_status with noServiceFound

                                    $data['request_status']=$status;
                                    $result=$ServiceRequestService->accessUpdateRideStatus((object)$data);
                                    break;
                                }

                                /*** End Driver Not Found */

                                /**
                                 * * Driver found!! Ya Ya.. :)
                                 * * Now insert the driver and request details in the service request log
                                 * * and  update the driver profile service status as requested
                                 * * Now send the response as RideSearch
                                 * *
                                 * */
                                //print_r(($driverDetails["data"][0])->user_id);
                                 $data["driver_id"]=($driverDetails["data"][0])->user_id;
                                 $DriversProfileService=new DriversProfileService();

                                 $driverProfile=$DriversProfileService->accessSetRequestToDriver((object)$data);
                                 $status="RIDESEARCH";
                                 $messsage="Connecting with near by drivers.";
                                 if($driverProfile['statusCode']!=200){
                                     break;
                                    }
                                    else if($driverProfile['statusCode']==200){
                                        // push
                                        $pushDate=[
                                            "title"=>trans("api.NOTIFICATION.TITLE.RIDE_REQUEST"),
                                            "text"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REQUEST"),
                                            "body"=>trans("api.NOTIFICATION.MESSAGE.RIDE_REQUEST"),
                                            "type"=>"RIDEREQUEST",
                                            "user_id"=>$data["driver_id"],
                                            "setTo"=>'SINGLE',
                                            "request_id"=>$data["request_id"]
                                        ];
                                        $NotificationServices =new NotificationServices();
                                        $Notification=new NotificationService();
                                        $Notification->accessCreateNotification((object)$pushDate);
                                        $NotificationServices->sendPushNotification((object)$pushDate);
                                        // send sms
                                       // $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                                        // email
                                    }
                                    $reqSer=$requestServices->accessInsertSerReqLog((object)$data);
                                }
                            }
                        break;

                        case "ACCEPTED":
                            $rideDetails=$this->requestedRide($val);
                            $rideDetails['estimatedTimeToReached']=120;

                            $status="ACCEPTED";
                            $messsage="";
                        break;
                        case "REACHED":
                            $rideDetails=$this->requestedRide($val);

                            $status="REACHED";
                            $messsage="";
                        break;
                        case "STARTED":
                            $rideDetails=$this->requestedRide($val);

                            $status="STARTED";
                            $messsage="";
                        break;
                        case "DROP":
                            $rideDetails=$this->requestedRide($val);
                            $temp=[];
                            $temp=$rideDetails['requestDetials'];
                            $temp['staredFromSource_on']=$temp['started_from_source'];
                            $rideDetails['payable']=$this->calculateFinalPayable($temp);
                            $status="DROP";
                            $messsage="";
                        break;
                        case "PAYMENT":
                            $rideDetails=$this->requestedRide($val);
                            $temp=[];
                            $temp=$rideDetails['requestDetials'];
                            $temp['staredFromSource_on']=$temp['started_from_source'];
                            $rideDetails['payable']=$this->calculateFinalPayable($temp);
                            $status="PAYMENT";
                            $messsage="";
                        break;
                        case "RATING":
                            $message=trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED");
                            $status="RATING";
                            $nextStep="NORMAL";
                        break;
                        case "COMPLETED":
                            $rideDetails=$this->requestedRide($val);
                            $temp=[];
                            $temp=$rideDetails['requestDetials'];
                            $temp['staredFromSource_on']=$temp['started_from_source'];
                            $rideDetails['payable']=$this->calculateFinalPayable($temp);
                          //  $status="PAYMENT";
                           // $messsage="";
                            $message=trans("api.NOTIFICATION.MESSAGE.RIDE_COMPLETED");
                            $status="COMPLETED";
                            $nextStep="NORMAL";
                        break;
                }
            }
            $nearBy=[];

            if($status=="NORMAL"){
                // get nearby drivers
                $EstimatedFareService=new EstimatedFareService();
                $nearBy=$EstimatedFareService->accessGetNearByDriver($UserDevices);
                if(count((array)$nearBy['data'])>0){
                   $nearBy=$nearBy['data'][0];
                }
                else{
                    $nearBy=[];
                }
            }
            $Background=[
                "status"=>$status,
                "messsage"=>$messsage,
                "rideDetails"=>(object)$rideDetails,
                "nearByDriver"=>$nearBy
            ];
            return response(['message'=>"Passenger Background","data"=>(object)$Background,"errors"=>array("exception"=>["OK"],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

}
