<?php

namespace App\Http\Controllers\Api\Passenger;

use App\Http\Controllers\Controller;
use App\Model\Request\ServiceRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\EstimatedFareService;
use App\Services\ServiceRequestService;
use App\Services\LocationService;
use App\Services\SettingServices;
use App\Services\RequestServices;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\NotificationServices;
use App\Services\DriversServiceType;
use App\Services\TransactionLogService;
use App\Services\DriversProfileService;
use App\Services\UsersDevices;
use App\Services\PassengersProfileService;
use App\Services\NotificationService;


use Validator;



class RequestController extends Controller
{
    private function generateStaticMap($s_latitude,$s_longitude,$d_latitude,$d_longitude){
        try{
            $details = "https://maps.googleapis.com/maps/api/directions/json?origin=".$s_latitude.",".$s_longitude."&destination=".$d_latitude.",".$d_longitude."&mode=driving&key=".env('GOOGLE_MAP_KEY');

            $json = $this->curl($details);
            $details = json_decode($json, TRUE);
            $route_key = $details['routes'][0]['overview_polyline']['points'];
            $map_icon_start = asset('asset/img/marker-start.png');
            $map_icon_end = asset('asset/img/marker-end.png');

            $static_map = "https://maps.googleapis.com/maps/api/staticmap?".
            "autoscale=1".
            "&size=640x260".
            "&maptype=terrain".
            "&format=png".
            "&visual_refresh=true".
            "&markers=icon:".$map_icon_start."%7C".$s_latitude.",".$s_longitude.
            "&markers=icon:".$map_icon_end."%7C".$d_latitude.",".$d_longitude.
            "&path=color:0x000000|weight:2|enc:".$route_key.
            "&key=".env('GOOGLE_MAP_KEY');
            return ["static_map"=>$static_map,"route_key"=>$route_key];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'You cannot bid for this request anymore!',"errors"=>array("exception"=>["Invalid credentials"])],403);
        }

    }
    public function estimated(request $request){
        try{
            $rule=[
                'source' =>'required',
                'destination' => 'required',
                'waypoint' => 'sometimes',
                'preferences' => 'sometimes',
                'request_type'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->user_id=Auth::user()->id;
            $request->driver_id=null;
            $EstimatedFareService =new EstimatedFareService();
            $findLastPaymentOption=$EstimatedFareService->accesslastPayment($request);
            //  exit;
            $request->request_status="SERVICESEARCH";
            $request->locationDetails=["source"=>$request->source,"waypoint"=>$request->waypoint,"destination"=>$request->destination];
            $staticMap=$this->generateStaticMap($request->source["latitude"],$request->source["longitude"],$request->destination["latitude"],$request->destination["longitude"]);
            $request->static_map=$staticMap['static_map'];
            $request->route_key=$staticMap['route_key'];
            $ServiceRequest=new ServiceRequestService();
            $ServiceRequestResult=$ServiceRequest->accessCreateRequest($request);//echo 2; exit;
            $request->request_id=$ServiceRequestResult->request_id;
            $request->request_no="POWERRIDE"."/".date("ymd")."/".$request->request_id;
            $ServiceRequestResult=$ServiceRequest->accessUpdateRequestNo($request);
            $SettingServices=new SettingServices();

            /** Save the requested locations */

            $LocationService = new LocationService();
            $locationArray=[];
            // sources
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->source["longitude"],
                "latitude"=>$request->source["latitude"],
                "address"=>$request->source["address"],
                "types"=>"source",
                "orders"=>0
            );
            // waypoint
            foreach($request->waypoint as $key=>$val){
                $locationArray[]=array(
                    "request_id"=>$request->request_id,
                    "longitude"=>$val["longitude"],
                    "latitude"=>$val["latitude"],
                    "address"=>$val["address"],
                    "types"=>"waypoint",
                    "orders"=>$val["order"]
                );
            }
            // destination
            $locationArray[]=array(
                "request_id"=>$request->request_id,
                "longitude"=>$request->destination["longitude"],
                "latitude"=>$request->destination["latitude"],
                "address"=>$request->destination["address"],
                "types"=>"destination",
                "orders"=>0
            );
            $LocationServiceReturn=$LocationService->accessCreateLocation($locationArray);
            $request->locationDetails=$locationArray;










           $retunEsti=$EstimatedFareService->accessGetFare($request);


         //  $retunEsti['data']['isFemaleFriendly']="Y";

           // print_r($retunEsti['message']);

            //accessGetFare
            return response(['message'=>$retunEsti['message'],"data"=>$retunEsti['data'],"last_paid_by"=>$findLastPaymentOption,"isFemaleFriendlyFeatureOn"=>$SettingServices->getValueByKey("female_friendly"),"errors"=>$retunEsti['errors']],$retunEsti['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function requestRide(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'service_type_id' => 'required',
                'isFemaleFriendly'=>'required|in:Y,N',
                'payment_method'=>'required|in:CASH,CARD',
                'card_id'=>['required_if:payment_method,CARD']
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request->request_status="RIDESEARCH";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);

          //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="RIDENOW"){
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, we are connecting with near by drivers.";
                    break;
                    case "RIDESEARCH":
                        $request_status=$getRequestData->request_status;
                        $message="We are connecting with near by drivers, we appreciate your patience.";
                    break;
                    }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
    public function ratingComment(Request $request){
        try{
            $rule=[
                'request_id' =>'required',
                'rating'=>'required',
                'comment'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);

            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->passenger_id=Auth::user()->id;
            $ServiceRequestService=new ServiceRequestService();

            $ServiceRequestServiceReturn = $ServiceRequestService->accessAddPassengerRatingComment((object)[
                "request_id"=>$request->request_id,
                "rating_by_passenger"=>$request->rating,
                "comment_by_passenger"=>$request->comment,
                "passenger_rating_status"=>"GIVEN",
                "passenger_id"=>$request->user_id
                ]);
            // calculate over all ratting.



            $message="Thank you, for rating the driver.";
            $status="RATING";
            $nextStep="COMPLETED";

            return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["ok"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    public function scheduleRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $request['ApplicationType']=$timeZone=$request->header("ApplicationType");

            $rule=[
                'request_id' =>'required|exists:service_requests,request_id',
                'service_type_id' => 'required',
                'payment_method'=>'required|in:CASH,CARD',
                'card_id'=>'required_if:payment_method,CARD',
                'date'=>'required',
                'time'=>'required',
                'comment'=>'sometimes',
                'isFemaleFriendly'=>'required|in:Y,N',
                'timeZone'=>'required',
                'ApplicationType'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $message="";
            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request_status= $request->request_status="SERVICESEARCH";

            // Log::info("json_encode(request)");
            // Log::info(json_encode($request->date));
            // Log::info(json_encode($request->time));
            // Log::info("json_encode(request)");

            //exit;

            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
            //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="RIDENOW"){
                // echo $getRequestData->request_status;
                // echo "sd"; exit;
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $getRequestData->request_type="SCHEDULE";
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        //  $ServiceRequest->accessUpdatePayment($request);
                        $result->request_type=$getRequestData->request_type;
                        // save schedule date time in utc
                        // check its from web or not




                        $result->schedule_datetime=$this->convertToUTC($request->date." ".$request->time,$request['timeZone']);
                        // Log::info("result->schedule_datetime");
                        // Log::info(json_encode($result->schedule_datetime));
                        // Log::info("result->schedule_datetime".$request['timeZone']);



                        $result->save();
                        $request_status=$request->request_status;
                        $message="Thank you for choosing us, nearby drivers will contact you on your scheduled date & time";
                    break;
                }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    public function scheduleCancel(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $request['ApplicationType']=$timeZone=$request->header("ApplicationType");
            $rule=[
                'request_id' =>'required|exists:service_requests,request_id',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $message="";
            $request->passenger_id=Auth::user()->id;
            $request->driver_id=null;
            $request_status= $request->request_status="CANCELBYPASSENGER";
            $ServiceRequest=new ServiceRequestService();
            // get the data from request table
            $getRequestData=$ServiceRequest->accessGetRequestByID($request);
            //  print_r($getRequestData); exit;
            if($getRequestData->request_type=="SCHEDULE"){
                switch ($getRequestData->request_status) {
                    case "SERVICESEARCH":
                        $getRequestData->request_type="SCHEDULE";
                        $result=$ServiceRequest->accessUpdateRideStatus($request);
                        $result->request_type=$getRequestData->request_type;
                        $request_status=$request->request_status;
                        $message="Your Schedule is been canceled successfully.";
                    break;
                }
            }
            return response(['message'=>$message,"data"=>(object)["request_status"=>$request_status,"request_id"=>$request->request_id],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }

    public function sendScheduleRideNotification(){
        try{
            $minutes_to_add = 1;
            $currentDateTime = date("Y-m-d H:i",strtotime("+10 minutes",strtotime(date('Y-m-d H:i'))));
            $addedOneMin = new \DateTime($currentDateTime);
            $addedOneMin->add(new \DateInterval('PT' . $minutes_to_add . 'M'));
            $addedOneMin = $addedOneMin->format('Y-m-d H:i');
            // Log::info("between ".$currentDateTime." and".$addedOneMin);
            $RequestServices = new RequestServices();
            $ServiceRequest=new ServiceRequestService();
            $NotificationServices =new NotificationServices();
            // find all the schedule ride
            $allSchedule= $RequestServices->accessGetAllActiveSchedule((object)["currentDateTime"=>$currentDateTime,"addedOneMin"=>$addedOneMin]);
            // print_r($allSchedule); exit;
            // Log::info(json_encode($allSchedule));
            foreach($allSchedule as $key=>$val){
                if($val['request_type']=="SCHEDULE"){
                    // Log::info("I am in the schedule blog");
                    switch ($val['request_status']) {
                        case "SERVICESEARCH":
                            // Log::info("I am in the SERVICESEARCH blog");
                            $request=['request_status'=>"RIDESEARCH",'request_id'=>$val['request_id'],'isFemaleFriendly'=>$val['isFemaleFriendly'],"passenger_id"=>$val['passenger_id']];
                            // check for RIDESEARCH ACCEPTED REACHED STARTED DROP PAYMENT RATING
                            $isOnRide=$ServiceRequest->accessCheckOnRide((object)$request);
                            if(!empty($isOnRide)&&count($isOnRide)>0){
                                //cancel the schedule ride
                                $result=$ServiceRequest->accessAutoCancelSchedule((object)$request);
                                // push
                                $pushDate=[
                                    "title"=>trans("api.NOTIFICATION.TITLE.SCHEDULED_RIDE_CANCEL"),
                                    "text"=>trans("api.NOTIFICATION.MESSAGE.SCHEDULED_RIDE_CANCEL"),
                                    "body"=>trans("api.NOTIFICATION.MESSAGE.SCHEDULED_RIDE_CANCEL"),
                                    "type"=>"SCHEDULEDRIDECANCEL",
                                    "user_id"=>$val['passenger_id'],
                                    "setTo"=>'SINGLE'
                                    ];
                                    $NotificationServices->sendPushNotification((object)$pushDate);
                                    // send sms
                                $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                                $Notification=new NotificationService();
                                $Notification->accessCreateNotification((object)$pushDate);
                                // email
                            }
                            else{
                                $result=$ServiceRequest->accessUpdateRideStatusSetSchedule((object)$request);
                            }
                        break;
                    }
                }
            }
            return true;
        }
        catch(\Illuminate\Database\QueryException  $e){
            // Log::info(json_encode($e));
            return false;
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            // Log::info(json_encode($e));
            return false;
        }
        catch(ModelNotFoundException $e){
            // Log::info(json_encode($e));
            return false;
        }
        catch (\Exception $e){
            // Log::info(json_encode($e));
            return false;
        }
    }

    public function tripHistory(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByPassengerId($request);
           // print_r($requestBooking); exit;
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }
                $requestBooking["data"][$key]['created_at']=$this->checkNull($requestBooking["data"][$key]['created_at']);
                if($requestBooking["data"][$key]['created_at']!=""){
                    $requestBooking["data"][$key]['created_at']=$this->convertFromUTC($requestBooking["data"][$key]['created_at'],$request->timeZone);
                }
                if($requestBooking["data"][$key]['request_status']=="CANCELBYPASSENGER"){
                    $requestBooking["data"][$key]['started_from_source']=$requestBooking["data"][$key]['created_at'];
                }


                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking["data"][$key]['request_id']]);
               // dd($tracationsReturn); exit;

                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];

                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }



                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');


                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking["data"][$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function upCommingScheduleTrip(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;

            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $requestBooking=$RequestServices->accessGetAllUpComingSchuduleRequestByPassengerId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            $DriversServiceType = new DriversServiceType();
            foreach($requestBooking["data"] as $key=>$val){
                $requestBooking["data"][$key]['schedule_datetime']=$this->checkNull($requestBooking["data"][$key]['schedule_datetime']);
                if($requestBooking["data"][$key]['schedule_datetime']!=""){
                    $requestBooking["data"][$key]['schedule_datetime']=$this->convertFromUTC($requestBooking["data"][$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking["data"][$key]['started_from_source']=$this->checkNull($requestBooking["data"][$key]['started_from_source']);
                if($requestBooking["data"][$key]['started_from_source']!=""){
                    $requestBooking["data"][$key]['started_from_source']=$this->convertFromUTC($requestBooking["data"][$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking["data"][$key]['dropped_on_destination']=$this->checkNull($requestBooking["data"][$key]['dropped_on_destination']);
                if($requestBooking["data"][$key]['dropped_on_destination']!=""){
                    $requestBooking["data"][$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking["data"][$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking["data"][$key]['accepted_on']=$this->checkNull($requestBooking["data"][$key]['accepted_on']);
                if($requestBooking["data"][$key]['accepted_on']!=""){
                    $requestBooking["data"][$key]['accepted_on']=$this->convertFromUTC($requestBooking["data"][$key]['accepted_on'],$request->timeZone);
                }

                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking["data"][$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking["data"][$key];

                $driverSer=$DriversServiceType->getServiceForScheduleHistory((object)['service_type_id'=>$requestBooking["data"][$key]['service_type_id']]);
               $myBooking[$key]['driverSer']=$driverSer['data'];


            }
            $requestBooking["data"]= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


          //  print_r($requestHistory); exit;





           // return response(['message'=>$message,"data"=>(object)["status"=>$status,"nextStep"=>$nextStep],"errors"=>array("exception"=>["Decline"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function tripDetails(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get all the complete ride  with user infomation
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $DriversProfileService=new DriversProfileService();
            $requestBooking=$RequestServices->accessGetAllCompleteRequestByPassengerId($request);
            $myBooking=[];
            $LocationService=new LocationService();
            foreach($requestBooking as $key=>$val){
                $requestBooking[$key]['schedule_datetime']=$this->checkNull($requestBooking[$key]['schedule_datetime']);
                if($requestBooking[$key]['schedule_datetime']!=""){
                    $requestBooking[$key]['schedule_datetime']=$this->convertFromUTC($requestBooking[$key]['schedule_datetime'],$request->timeZone);
                }
                $requestBooking[$key]['started_from_source']=$this->checkNull($requestBooking[$key]['started_from_source']);
                if($requestBooking[$key]['started_from_source']!=""){
                    $requestBooking[$key]['started_from_source']=$this->convertFromUTC($requestBooking[$key]['started_from_source'],$request->timeZone);
                }
                $requestBooking[$key]['dropped_on_destination']=$this->checkNull($requestBooking[$key]['dropped_on_destination']);
                if($requestBooking[$key]['dropped_on_destination']!=""){
                    $requestBooking[$key]['dropped_on_destination']=$this->convertFromUTC($requestBooking[$key]['dropped_on_destination'],$request->timeZone);
                }
                $requestBooking[$key]['accepted_on']=$this->checkNull($requestBooking[$key]['accepted_on']);
                if($requestBooking[$key]['accepted_on']!=""){
                    $requestBooking[$key]['accepted_on']=$this->convertFromUTC($requestBooking[$key]['accepted_on'],$request->timeZone);
                }
                $requestBooking[$key]['created_at']=$this->checkNull($requestBooking[$key]['created_at']);
                if($requestBooking[$key]['created_at']!=""){
                    $requestBooking[$key]['created_at']=$this->convertFromUTC($requestBooking[$key]['created_at'],$request->timeZone);
                }
                if($requestBooking[$key]['request_status']=="CANCELBYPASSENGER"){
                    $requestBooking[$key]['started_from_source']=$requestBooking[$key]['created_at'];
                }
                $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

                $locDetails=$LocationService->accessGetLocation($requestBooking[$key]['request_id']);

                foreach($locDetails as $locKey =>$locVal){
                    switch($locVal['types']){
                        case "source":
                            $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "destination":
                            $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                        case "waypoint":
                            $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                        break;
                    }
                }

                $myBooking[$key]['locDetailsArray']= $locDetailsArray;
                $myBooking[$key]['requestDetails']=$requestBooking[$key];
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$requestBooking[$key]['request_id']]);
                $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$requestBooking[$key]['request_id']]);
               //print_r($cancelAmount); exit;
                $myBooking[$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $myBooking[$key]['transaction']->cost=(float)$myBooking[$key]['transaction']->cost-(float)$myBooking[$key]['transaction']->promo_code_value;
                if($myBooking[$key]['transaction']->cost<=0){
                    $myBooking[$key]['transaction']->cost=0.00;
                }
                $myBooking[$key]['transaction']->cancelFee=(float)$cancelAmount['data'];
                $myBooking[$key]['transaction']->cost= number_format((float)($myBooking[$key]['transaction']->cost), 2, '.', '');
                $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$requestBooking[$key]['driver_id']]);
                $myBooking[$key]['driverSer']=$driverSer['data'];
                $ProfileData=$DriversProfileService->accessGetProfile((object)['user_id'=>$requestBooking[$key]['driver_id']]);

                $myBooking[$key]['ProfileData']=$ProfileData['data'];


            }
            $requestBooking= $myBooking;
            return response(['message'=>"Done","data"=>(object)$requestBooking[0],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function rideShareDetails(Request $request){

        try{
            //$request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'ride'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };


            $request->request_id= (int) $request->ride;

            $request->user_id=Auth::user()->id;




            // get all the complete ride  with user infomation
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $DriversProfileService=new DriversProfileService();
            $LocationService=new LocationService();
            $UsersDevices=new UsersDevices();
            $PassengersProfileService=new PassengersProfileService();

            $getRequest=$ServiceRequestService->accessGetRequestByID($request);
            $locDetails=$LocationService->accessGetLocation($request->request_id);
            $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

            foreach($locDetails as $locKey =>$locVal){
                switch($locVal['types']){
                    case "source":
                        $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                    case "destination":
                        $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                    case "waypoint":
                        $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                }
            }

            $ProfileData=$DriversProfileService->accessGetProfile((object)['user_id'=>$getRequest->driver_id]);

             $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$getRequest->driver_id]);
             $DeiverDevice=$UsersDevices->accessGetDevice((object)['user_id'=>$getRequest->driver_id]);

             $PassengerProfile=$PassengersProfileService->accessGetProfile((object)['user_id'=>$getRequest->passenger_id]);






           $requestBooking['DriverProfileData']['first_name']= $ProfileData['data']['first_name'];
           $requestBooking['DriverProfileData']['last_name']= $ProfileData['data']['last_name'];
           $requestBooking['DriverProfileData']['mobile_no']= $ProfileData['data']['mobile_no'];
           $requestBooking['DriverProfileData']['isd_code']= $ProfileData['data']['isd_code'];
           $requestBooking['DriverProfileData']['picture']= $ProfileData['data']['picture'];

           $requestBooking['PassengerProfileData']['first_name']= $PassengerProfile['data']['first_name'];
           $requestBooking['PassengerProfileData']['last_name']= $PassengerProfile['data']['last_name'];
           $requestBooking['PassengerProfileData']['mobile_no']= $PassengerProfile['data']['mobile_no'];
           $requestBooking['PassengerProfileData']['isd_code']= $PassengerProfile['data']['isd_code'];
           $requestBooking['PassengerProfileData']['picture']= $PassengerProfile['data']['picture'];


           $requestBooking['location']= $locDetailsArray;
           $requestBooking['driverService']= $driverSer['data'];
           $requestBooking['driverLocation']['latitude']=$DeiverDevice['latitude'];
           $requestBooking['driverLocation']['longitude']=$DeiverDevice['longitude'];



            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function rideShareDetailsA(Request $request){

        try{
            //$request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'ride'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $useragent=$_SERVER['HTTP_USER_AGENT'];
            if(!preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))){
               // abort(404);
            }


            $request->request_id= (int) $request->ride;

            //$request->user_id=Auth::user()->id;




            // get all the complete ride  with user infomation
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $TransactionLogService=new TransactionLogService();
            $DriversServiceType = new DriversServiceType();
            $DriversProfileService=new DriversProfileService();
            $LocationService=new LocationService();
            $UsersDevices=new UsersDevices();
            $PassengersProfileService=new PassengersProfileService();

            $getRequest=$ServiceRequestService->accessGetRequestByID($request);
            $locDetails=$LocationService->accessGetLocation($request->request_id);
            $locDetailsArray=["source"=>(object)[],"waypoints"=>[],"destination"=>(object)[]];

            foreach($locDetails as $locKey =>$locVal){
                switch($locVal['types']){
                    case "source":
                        $locDetailsArray["source"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                    case "destination":
                        $locDetailsArray["destination"]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                    case "waypoint":
                        $locDetailsArray["waypoints"][]=(object)["longitude"=>(float)$locVal['longitude'],"latitude"=>(float)$locVal['latitude'],"address"=>$locVal['address'],"orders"=>$locVal['orders']];
                    break;
                }
            }

            $ProfileData=$DriversProfileService->accessGetProfile((object)['user_id'=>$getRequest->driver_id]);

             $driverSer=$DriversServiceType->accessGetServiceHistory((object)['user_id'=>$getRequest->driver_id]);
             $DeiverDevice=$UsersDevices->accessGetDevice((object)['user_id'=>$getRequest->driver_id]);

             $PassengerProfile=$PassengersProfileService->accessGetProfile((object)['user_id'=>$getRequest->passenger_id]);






           $requestBooking['DriverProfileData']['first_name']= $ProfileData['data']['first_name'];
           $requestBooking['DriverProfileData']['last_name']= $ProfileData['data']['last_name'];
           $requestBooking['DriverProfileData']['mobile_no']= $ProfileData['data']['mobile_no'];
           $requestBooking['DriverProfileData']['isd_code']= $ProfileData['data']['isd_code'];
           $requestBooking['DriverProfileData']['picture']= $ProfileData['data']['picture'];

           $requestBooking['PassengerProfileData']['first_name']= $PassengerProfile['data']['first_name'];
           $requestBooking['PassengerProfileData']['last_name']= $PassengerProfile['data']['last_name'];
           $requestBooking['PassengerProfileData']['mobile_no']= $PassengerProfile['data']['mobile_no'];
           $requestBooking['PassengerProfileData']['isd_code']= $PassengerProfile['data']['isd_code'];
           $requestBooking['PassengerProfileData']['picture']= $PassengerProfile['data']['picture'];


           $requestBooking['location']= $locDetailsArray;
           $requestBooking['driverService']= $driverSer['data'];
           $requestBooking['driverLocation']['latitude']=$DeiverDevice['latitude'];
           $requestBooking['driverLocation']['longitude']=$DeiverDevice['longitude'];



            return response(['message'=>"Done","data"=>(object)$requestBooking,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function appleAppSiteAssociation(Request $request){

        try{


                $array=["applinks"=>["details"=>["appIDs"=> [ "2H5JW86X3B.com.entro.passenger-ios"]]]];

            return response($array,200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }

    }

    public function cancelRide(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'request_id'=>'required'
            ];


            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_id=Auth::user()->id;
            // get request details,
            $ServiceRequestService=new ServiceRequestService();
            $RequestServices=new RequestServices();
            $DriverProfile=new DriversProfileService();
            $SettingServices=new SettingServices();
            $TransactionLogService=new TransactionLogService();
            $NotificationServices =new NotificationServices();

            $getRequestByID=$ServiceRequestService->accessGetRequestByID($request);
            //print_r($getSerReqLog); exit;
            if($getRequestByID->request_status=="SERVICESEARCH" || $getRequestByID->request_status=="RIDESEARCH" ){
                $getRequestByID->request_status="CANCELBYPASSENGER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="REQUESTED"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYPASSENGER"],$status="REQUESTED");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="REQUESTED");
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "type"=>"CANCELBYPASSENGER",
                            "user_id"=>$val['driver_id'],
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                    }
                }
                $message=trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_PASSENGER");
                $pushDate=[
                    "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                    "text"=>$message,
                    "body"=>$message,
                    "type"=>"CANCELBYPASSENGER",
                    "user_id"=>$getRequestByID->passenger_id,
                    "setTo"=>'SINGLE'
                    ];
                    $NotificationServices->sendPushNotification((object)$pushDate);
                    $NotificationServices->accessSendSMSToPassenger((object)$pushDate);

            }
            else if($getRequestByID->request_status=="ACCEPTED" || $getRequestByID->request_status=="REACHED")
            {
                $getRequestByID->request_status="CANCELBYPASSENGER";
                $getRequestByID->save();
                // get service log object
                $getSerReqLog=$RequestServices->accessGetSerReqLog($getRequestByID);
                foreach($getSerReqLog as $key=>$val){
                    if($val['status']=="ONRIDE"){
                        // update the driver profile status and service log status
                        $updateServiceLogStatus=$RequestServices->accessUpdateStatus((object)["request_id"=>$val['request_id'],"driver_id"=>$val['driver_id'],"status"=>"CANCELBYPASSENGER"],$status="ONRIDE");
                        $updateProfile=$DriverProfile->accessSetRequestToDriver((object)['driver_service_status'=>"ACTIVE","driver_id"=>$val['driver_id']],$service_status="ONRIDE");
                    }
                }
                $cancelAmount=$SettingServices->getValueByKey("passenger_cancellation_charge");
                $cancelationCharger=[
                "request_id"=>$request->request_id,
                'ride_insurance'=>0,
                'per_minute'=>0,
                'per_distance_km'=>0,
                'minimum_waiting_time_in_minutes'=>0,
                'waiting_charge_per_min'=>0,
                'waitTime'=>0,
                'duration_hr'=>0,
                'duration_min'=>0,
                'duration_sec'=>0,
                'duration'=>0,
                'distance_km'=>0,
                'distance_miles'=>0,
                'distance_meters'=>0,
                "cost"=>$cancelAmount,
                "payment_method"=>"NONE",
                "payment_gateway_charge"=>0,
                'types'=>'CREDIT',
                "transaction_type"=>"CANCELBYPASSENGERCHARGE",
                "is_paid"=>"N",
                "status"=>"PENDING"
                ];
                $TransactionLogService->createCreditNoteForRide($cancelationCharger);
                $message= trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_BY_PASSENGER_WITH_FEE", [ 'cancelAmount' =>$cancelAmount, 'currency' => '₦' ]);  //"Your request has been cancelled and $cancelAmount ₦ will be charged from your next ride as the cancellation fee";
                $pushDate=[
                        "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                        "text"=>$message,
                        "body"=>$message,
                        "type"=>"CANCELBYPASSENGER",
                        "user_id"=>$getRequestByID->passenger_id,
                        "setTo"=>'SINGLE'
                        ];
                        $NotificationServices->sendPushNotification((object)$pushDate);
                        $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
                        $pushDate=[
                            "title"=>trans("api.NOTIFICATION.TITLE.REQUEST_CANCLED"),
                            "text"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "body"=>trans("api.NOTIFICATION.MESSAGE.REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER"),
                            "type"=>"CANCELBYPASSENGER",
                            "user_id"=>$getRequestByID->driver_id,
                            "setTo"=>'SINGLE'
                            ];
                            $NotificationServices->sendPushNotification((object)$pushDate);
                            $NotificationServices->accessSendSMSToPassenger((object)$pushDate);
            }
            else{
                $message="It's seems your onride already, you cancel ride cannot be processed.";
            }
            return response(['message'=>$message,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])],200);


        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e)],400);
        }
    }
}
