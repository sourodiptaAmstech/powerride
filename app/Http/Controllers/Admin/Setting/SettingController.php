<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Setting\Setting;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // ->orWhere('key','web_site')->orWhere('key','contact_number')->orWhere('key','contact_email')
        $site_settings = Setting::where('key','site_title')->orWhere('key','site_logo')->orWhere('key','site_email_logo')->orWhere('key','site_icon')->orWhere('key','site_copyright')->orWhere('key','female_friendly')->orWhere('key','commission')->orWhere('key','passenger_cancellation_charge')->orWhere('key','driver_cancellation_charge')->orWhere('key','contact_number')->orWhere('key','contact_email')->orWhere('key','sos_number')->get();

        return view('admin.setting.index', compact('site_settings'));
    }

    public function femaleFriendlyUpdate(Request $request)
    {
        if ($request->status=='YES'){
            $setting = Setting::where('key','female_friendly')->first();
            $setting->value = 'Y';
            $setting->save();
        }
        if ($request->status=='NO'){
            $setting = Setting::where('key','female_friendly')->first();
            $setting->value = 'N';
            $setting->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Updated Successfully.',
            'data' => $setting,
        ], 200);
    }

    public function getFemaleFriendly(Request $request)
    {
        $setting = Setting::where('key','female_friendly')->first();
        return response()->json([
            'success' => true,
            'message' => 'Get female friendly.',
            'data' => $setting,
        ], 200);
    }
    
    public function privacyPolicy()
    {
        $policies = Setting::where('key','condition_privacy')->orWhere('key','page_privacy')->orWhere('key','page_privacy_driver')->orWhere('key','condition_privacy_driver')->orWhere('key','legal')->orWhere('key','legal_driver')->get();
        return view('admin.setting.policy', compact('policies'));
    }

    public function privacyPolicyUpdate(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);

            if ($site_setting->key == 'condition_privacy'||$site_setting->key == 'page_privacy'||$site_setting->key == 'page_privacy_driver'||$site_setting->key == 'condition_privacy_driver'||$site_setting->key == 'legal'||$site_setting->key == 'legal_driver') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            }
            $site_setting->save();
            return redirect()->route('admin.privacy.policy')->with('flash_success', 'Privacy Policy Updated Successfully');    
        }
        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Privacy Policy Not Found');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            return view('admin.setting.edit',compact('site_setting'));
        } catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $site_setting = Setting::findOrFail($id);
            if ($request->key == 'site_title'||$request->key == 'site_copyright'||$site_setting->key == 'web_site'||$site_setting->key == 'contact_number'||$site_setting->key == 'contact_email'||$site_setting->key == 'sos_number'||$site_setting->key == 'commission'||$site_setting->key == 'passenger_cancellation_charge'||$site_setting->key == 'driver_cancellation_charge') {
                $this->validate($request, [
                    'value' => 'required',
                ]);
                $site_setting->value = $request->value;
            } else {
                $this->validate($request, [
                    'value' => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                ]);
                if(isset($request->value) && !empty($request->value)){
                    //$Storage=Storage::delete($site_setting->value);
                    $value = $request->value->store('public/setting/'.$id);
                    $value=str_replace("public", "storage", $value);
                    $site_setting->value=$value;
                }
            }
            $site_setting->save();
            return redirect()->route('admin.setting.index')->with('flash_success', 'Site Setting Updated Successfully');    
        } 

        catch (ModelNotFoundException $e) {
            return back()->with('flash_error', 'Site Setting Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
