<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\DriverProfiles;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;

// use App\Model\Driver\DriverServices;
use App\Model\ServiceType;
use App\Model\Document\DriverDocuments;
use App\Model\Document\Document;
use App\Model\Driver\DriverCarImages;
use App\Http\Controllers\Api\Transaction\TransactionController;

use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailNotification;
use App\Notifications\EmailAccountNotification;

use App\Model\Driver\DriverDocumentReason;
use App\Model\Driver\DriverReason;

use App\Model\ServiceType\DriverServiceType;
use App\Model\Request\ServiceRequest;
use App\Services\ServiceTypeMst;
use App\Model\Vehicle\Vehicle;
use App\Services\UsersDevices;
use App\Services\DriversServiceType;
use App\Services\VehicleService;
use App\Services\TwilioSMS;
use App\Model\Setting\Setting;



class DriverController extends Controller
{

    public function index()
    {
        return view('admin.driver.index');
    }

    public function ajaxDriver(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'email_id',
            3 => 'isd_code',
        );

        $totalData =  User::where('user_scope', 'driver-service')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='id') {
                $drivers = User::join('drivers_profile as dr','users.id','=','dr.user_id')
                    ->where('users.user_scope', 'driver-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('users.'.$order,$dir)
                    ->get();
            } else {
                $drivers = User::join('drivers_profile as dr','users.id','=','dr.user_id')
                    ->where('users.user_scope', 'driver-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('dr.'.$order,$dir)
                    ->get();
            }

            $totalFiltered = User::where('user_scope', 'driver-service')->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='id') {
                $drivers = User::join('drivers_profile as dr','users.id','=','dr.user_id')
                ->where('users.user_scope', 'driver-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('dr.first_name', 'like', "%{$search}%")
                    ->orWhere('dr.last_name','like',"%{$search}%")
                    ->orWhere('dr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('dr.isd_code','like',"%{$search}%")
                    ->orWhere('dr.mobile_no','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('users.'.$order, $dir)
                ->get();
            } else {
                $drivers = User::join('drivers_profile as dr','users.id','=','dr.user_id')
                ->where('users.user_scope', 'driver-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('dr.first_name', 'like', "%{$search}%")
                    ->orWhere('dr.last_name','like',"%{$search}%")
                    ->orWhere('dr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('dr.isd_code','like',"%{$search}%")
                    ->orWhere('dr.mobile_no','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('dr.'.$order, $dir)
                ->get();
            }

            $totalFiltered = User::join('drivers_profile as dr','users.id','=','dr.user_id')
                ->where('users.user_scope', 'driver-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('dr.first_name', 'like', "%{$search}%")
                    ->orWhere('dr.last_name','like',"%{$search}%")
                    ->orWhere('dr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('dr.isd_code','like',"%{$search}%")
                    ->orWhere('dr.mobile_no','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($drivers){
            foreach($drivers as $d){
                if (isset($d->driver_profile)) {
                    if($d->driver_profile->service_status == "INACTIVE" || $d->driver_profile->service_status == "BLOCK"){
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-danger driver-status" data-toggle="tooltip" data-value="Activate" data-id="'.$d->id.'" title="Click to Active">Inactive</a>';
                    }else if ($d->driver_profile->service_status == "REQUESTED"){
                        $activationButton = '<a href="javascript:void(0)" class="btn btn-warning">Requested</a>';
                    }else if ($d->driver_profile->service_status == "ONRIDE"){
                        $activationButton = '<a href="javascript:void(0)" class="btn btn-warning">Onride</a>';
                    }else{
                        $activationButton = '<a href="driver/activation/'.$d->id.'" class="btn btn-success driver-status" data-toggle="tooltip" data-value="Inactivate" data-id="'.$d->id.'" title="Click to Inactive">Active</a>';
                    }
                }else{
                    $activationButton = null;
                }

                $nestedData['id']     = $d->id;
                $nestedData['driver_name']   = $d->driver_profile->first_name." ".$d->driver_profile->last_name;
                $nestedData['email']   = $d->driver_profile->email_id;
                $nestedData['mobile']  = $d->driver_profile->isd_code.'-'.$d->driver_profile->mobile_no;
                $nestedData['action'] = '<span style="line-height: 38px;"><a href="driver/'.$d->id.'/edit" class="btn btn-info">Edit</a> <a href="drivers/document/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Document</a> <a href="drivers/service-type/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Service</a>
                    <a href="drivers/review-rating/'.$d->id.'" class="btn btn-info"> Review/Rating</a> <a href="driver/trip/history/'.$d->id.'" class="btn btn-info">Trip History</a> <a href="driver/transaction/'.$d->id.'" class="btn btn-info">Transaction</a>
                    </span>
                    '.$activationButton.'';
                    // <a href="driver/carimages/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Car Images</a>
                    // <a href="driver/transaction/'.$d->id.'" class="btn btn-info"><i class="fa fa-file"></i> Package</a>

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    public function listDriverServiceType($id)
    {
        try{
            $ServiceType=DriverServiceType::select('user_id','service_type_id','registration_no','registration_expire','model','vechile_identification_no','driver_service_type_id','model_year','make','updated_at','created_at')->where("user_id",$id)->first();
            if (isset($ServiceType)){
                $ServiceTypeMst=new ServiceTypeMst();
                $ServiceTypeMstData=$ServiceTypeMst->accessGetNameByID($ServiceType);
                $ServiceType->serviceName=$ServiceTypeMstData->name;
                return view('admin.driver.service',compact('ServiceType'));
            } else {
                 return redirect()->back()->with('flash_error', 'Service type not found!');
            }
        }
        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(ModelNotFoundException $e){
            return redirect()->back()->with('flash_error', 'You are not registered with us!');
        }
    }

    public function create()
    {
        $ServiceTypeMst=new ServiceTypeMst();
        $ServiceTypeMstDatas=$ServiceTypeMst->accessGet();
        return view('admin.driver.create',compact('ServiceTypeMstDatas'));
    }

    public function getVehicle(Request $request)
    {
        try{

            $VehicleService=new VehicleService();
            $VehicleService=$VehicleService->accessGet();
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$VehicleService['data'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


    public function getAdminMake(Request $request){
        try{
            $VehicleService=new VehicleService();
            $VehicleService=$VehicleService->accessGetMake();
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$VehicleService['data'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getAdminModel(Request $request){
        try{
            $rule=[
                'make'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $VehicleService=new VehicleService();
            $VehicleService=$VehicleService->accessGetModel($request);
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$VehicleService['data'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getAdminYear(Request $request){
        try{
            $rule=[
                'make'=>'required',
                'model'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            $VehicleService=new VehicleService();
            $VehicleService=$VehicleService->accessGetYear($request);
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SERVICE_LIST"),"data"=>$VehicleService['data'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


    public function store(Request $request)
    {
        try{
            $this->validate($request, [
                'first_name'        => 'required|max:255',
                'last_name'         => 'required|max:255',
                'email'             => 'nullable|email|max:255|unique:drivers_profile,email_id',
                'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
                'password'          => 'required|confirmed|min:6',
                'mobile'            => 'required|digits_between:5,10|unique:drivers_profile,mobile_no',
                'service_type_id'   => 'required',
                'car_make'          => 'required',
                'car_model'         => 'required',
                'model_year'        => 'required',
                'car_number'        => 'required',
                'is_vehicle_tinted' => 'required'
            ]);

            if (empty($request->mobile)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password=bcrypt(trim($request->password));
            $User->user_scope="driver-service";
            $User->username=$request->mobile;
            $User->save();

            $DriverProfiles=new DriverProfiles();
            $DriverProfiles->user_id=$User->id;
            $DriverProfiles->first_name=$request->first_name;
            $DriverProfiles->last_name=$request->last_name;
            $DriverProfiles->email_id = $request->email;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/profile');
                $picture = str_replace("public", "storage", $picture);
                $DriverProfiles->picture=$picture;
                $DriverProfiles->picture="http://104.131.120.166/powerride/public/".$picture;
            }
            $DriverProfiles->mobile_no=$request->mobile;
            $DriverProfiles->isd_code=$request->code;
            $DriverProfiles->dob=$request->dob;
            $DriverProfiles->gender=$request->gender;
            $DriverProfiles->isMobileverified=1;
            $DriverProfiles->save();

            //confirm it
            $request->user_id = $User->id;
            $request->device_id = "device_id";
            $request->device_token = "device_token";
            $UserDevice=new UsersDevices();
            $UserDevice->accessCreateDevices($request);

            $request->service_type_id = $request->service_type_id;
            $request->registration_no = $request->car_number;
            $request->model = $request->car_model;
            $request->model_year = (int)$request->model_year;
            $request->make = $request->car_make;
            $request->model_is_tinted = $request->is_vehicle_tinted;

            $DriversServiceType=new DriversServiceType();
            $DriversServiceType->accessCreate($request);

            $User->email = $request->email;
            $sub = "Login Credentials to Powerride";
            $message = "Please login to the powerride app with below login credentials.";
            $email = "Username: ".$request->code."-".$request->mobile;
            $password = "Password: ".$request->password;
            Notification::send($User, new EmailAccountNotification($sub,$message,$email,$password));

            if(!empty($User)){
                if($User->id>0){
                    return redirect()->route('admin.driver.index')->with('flash_success', 'Driver account created successfully.');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }

    }

    public function driverRatingReview($id){
        $driver = DriverProfiles::where('user_id',$id)->first();
        $serviceRequests = ServiceRequest::where('driver_id',$id)->get();
        $site_setting = Setting::where('key','female_friendly')->first();
        return view('admin.driver.review-rating',compact('driver','serviceRequests','site_setting'));
    }

    public function isFemaleFriendly(Request $request)
    {
        if ($request->status=='YES') {
            $DriverService = DriverServiceType::where('user_id',$request->driver_id)->first();
            $DriverService->isFemaleFriendly = 1;
            $DriverService->save();
        }
        if ($request->status=='NO') {
            $DriverService = DriverServiceType::where('user_id',$request->driver_id)->first();
            $DriverService->isFemaleFriendly = 0;
            $DriverService->save();
        }

        return response()->json([
            'success' => true,
            'message' => 'Updated Successfully.',
            'data' => $DriverService,
        ], 200);
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        try {
            $ServiceTypeMst=new ServiceTypeMst();
            $ServiceTypeMstDatas=$ServiceTypeMst->accessGet();
            $driver = User::find($id);
            return view('admin.driver.edit',compact('driver','ServiceTypeMstDatas'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }


    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'nullable|email|max:255|unique:drivers_profile,email_id,'.$id.',user_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|digits_between:5,10|unique:drivers_profile,mobile_no,'.$id.',user_id',
            'service_type_id'   => 'required',
            'car_number'        => 'required'
        ]);

        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $driver = User::find($id);
            $driver->username = $request->mobile;
            $driver->save();

            $DriverProfiles = DriverProfiles::where('user_id', $id)->first();
            $DriverProfiles->first_name    = $request->first_name;
            $DriverProfiles->last_name     = $request->last_name;
            $DriverProfiles->email_id      = $request->email;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/driver/'.$id.'/profile');
                $picture = str_replace("public", "storage", $picture);
                $DriverProfiles->picture="http://104.131.120.166/powerride/public/".$picture;
            }
            $DriverProfiles->mobile_no     = $request->mobile;
            $DriverProfiles->isd_code      = $request->code;
            $DriverProfiles->dob           =$request->dob;
            $DriverProfiles->gender        =$request->gender;
            $DriverProfiles->save();

            $ServiceType=DriverServiceType::where("user_id",$id)->first();
            $ServiceType->service_type_id=$request->service_type_id;
            $ServiceType->registration_no=$request->car_number;
            $ServiceType->save();

            return redirect()->route('admin.driver.index')->with('flash_success', 'Driver Updated Successfully');
        }

        catch (Exception $e) {
            return back()->with('flash_error', 'Driver Not Found');
        }
    }

    public function listDriverDocument($id)
    {
        $driver_documents = DriverDocuments::where('user_id', $id)->get();
        if ($driver_documents->count()>0) {
            return view('admin.driver.document', compact('driver_documents'));
        } else {
            return redirect()->back()->with('flash_error', 'Document not found!');
        }
    }

    public function driverDocumentVerification(Request $request,$id)
    {
        $user = User::find($request->user_id);
        $document = DriverDocuments::where('id', $request->document_id)->first();

        if ($request->status == 'ACTIVE') {
            $document->status = $request->status;
        }
        if ($request->status == 'INVALID') {
            $document->status = $request->status;
            // //send email
            // $msg="Your document ".$document->document->name." has been invalided by admin. The reason for invalidation is that ".$request->reason;
            $msg="Your document ".$document->document->name." has been invalided by admin.";
            $sub = 'Document Invalid Notification';
            $user->email = $user->driver_profile->email_id;
            Notification::send($user, new EmailNotification($msg,$sub));
            // //send sms
            $request->body = "Your document ".$document->document->name." has been invalided by admin.";
            $request->mobile_no = $user->driver_profile->mobile_no;
            $request->isdCode = $user->driver_profile->isd_code;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($request);
        }
        if ($request->status == 'EXPIRE') {
            $document->status = $request->status;
            // //send email
            // $msg="Your document ".$document->document->name." has been expired by admin. The reason for expiration is that ".$request->reason;
            $msg="Your document ".$document->document->name." has been expired by admin.";
            $sub = 'Document Expire Notification';
            $user->email = $user->driver_profile->email_id;
            Notification::send($user, new EmailNotification($msg,$sub));
            // //send sms
            $request->body = "Your document ".$document->document->name." has been expired by admin.";
            $request->mobile_no = $user->driver_profile->mobile_no;
            $request->isdCode = $user->driver_profile->isd_code;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($request);
        }
        $document->save();

        return response()->json([
            'success' => true,
            'message' => 'Document verified successfully'
        ], 200);

    }

    public function driverActivationProcess(Request $request,$id)
    {
        $user = User::find($id);
        $driver_profile = DriverProfiles::where('user_id', $id)->first();
        if ($user->driver_profile->service_status == 'ACTIVE') {
            $driver_profile->service_status = 'BLOCK';
            $driver_profile->save();
            // //send email
            $msg="Your Power Ride driver account has been deactivated by admin.";
            $sub = 'Power Ride Driver DeActivation Notification';
            $user->email = $driver_profile->email_id;
            Notification::send($user, new EmailNotification($msg,$sub));
            //send sms
            $request->body="Your Power Ride driver account has been deactivated by admin.";
            $request->mobile_no = $driver_profile->mobile_no;
            $request->isdCode = $driver_profile->isd_code;
            $TwilioSMS=new TwilioSMS();
            $TwilioSMSReturn=$TwilioSMS->accessSendSMS($request);

            return redirect()->back()->with('flash_success', 'Driver is inactivated successfully');
        } else {

           if (isset($driver_profile->isMobileverified)&&$driver_profile->isMobileverified != 0) {

                $driver_service = DriverServiceType::where('user_id', $id)->first();
                if (isset($driver_service)) {
                    if (isset($driver_service->registration_no)) {
                        // if ($driver_service->car_number_expire_date > date('Y-m-d')) {
                            // $documents = Document::where('status', 1)->get();

                            // if ($documents->count()>0) {

                                // foreach ($documents as $key => $document) {
                                // $document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->first();
                                //     if (!isset($document)) {
                                //         return redirect()->back()->with('flash_error', 'Not allow to activate driver! All the required documents are not uploaded. Please upload all the documents!');
                                //     }
                                // }

                                // foreach ($documents as $key => $document) {
                                // $active_document = DriverDocuments::where('user_id', $id)->where('document_id',$document->id)->where('status','ACTIVE')->first();
                                //     if (!isset($active_document)) {
                                //         return redirect()->back()->with('flash_error', 'Uploaded documents are not active document. Not allow to activate driver!');
                                //     }
                                // }

                                $driver_profile->service_status = 'ACTIVE';
                                $driver_profile->save();
                                //send email
                                $msg="Welcome! Your Power Ride driver account is activated successfully.";
                                $sub = 'Power Ride Driver Activation Notification';
                                $user->email = $driver_profile->email_id;
                                Notification::send($user, new EmailNotification($msg,$sub));
                                // //send sms
                                $request->body="Your Power Ride driver account has been activated by admin.";
                                $request->mobile_no = $driver_profile->mobile_no;
                                $request->isdCode = $driver_profile->isd_code;
                                $TwilioSMS=new TwilioSMS();
                                $TwilioSMSReturn=$TwilioSMS->accessSendSMS($request);

                                return redirect()->back()->with('flash_success', 'Driver is activated successfully');
                            // } else {
                            //     return redirect()->back()->with('flash_error', 'Document not found. Not allow to activate driver!');
                            // }

                        // } else {
                        //     return redirect()->back()->with('flash_error', 'Driver\'s car number has expired. Not allow to activate driver!');
                        // }
                    }else{
                        return redirect()->back()->with('flash_error', 'Car registration number not found! Not allow to activate driver');
                    }
                } else {
                    return redirect()->back()->with('flash_error', 'Driver service not found! Not allow to activate driver');
                }

            } else {
                return redirect()->back()->with('flash_error', 'Mobile number is not verified! Not allow to activate driver');
            }
        }
    }


    public function driverCarImages($id)
    {
        $service = DriverServices::where('user_id', $id)->first();
        if (!empty($service)) {
            $carimages = DriverCarImages::where('service_id', $service->service_id)->get();
            if ($carimages->count()>0) {
                return view('admin.driver.carimage', compact('carimages'));
            } else {
                return redirect()->back()->with('flash_error', 'Driver car images not found!');
            }
        } else {
            return redirect()->back()->with('flash_error', 'Driver service not found!');
        }
    }

    public function destroy($id)
    {
        //
    }

    public function driverTransaction($id)
    {
        $transaction_obj = new TransactionController;
        $transaction = $transaction_obj->getPackagesBrought($id);

        if($transaction['statusCode'] == 200){
            return view('admin.driver.package', compact('transaction'));
        } elseif ($transaction['statusCode'] == 500) {
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        } elseif ($transaction['statusCode'] == 400) {
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        } else {
            return redirect()->back()->with('flash_error', 'Your not authorized to access!');
        }
    }


    public function documentVerificationReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $reason = DriverDocumentReason::where('driver_id',$request->user_id)->where('document_id',$request->id)->first();
        if(empty($reason))
        {
            $reason = new DriverDocumentReason;
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
            }
            $reason->save();
        } else {
            $reason->driver_id = $request->user_id;
            $reason->document_id = $request->id;
            if ($request->status == 'INVALID') {
                $reason->reason_for_invalid = $request->reason;
                $reason->reason_for_expire = null;
            }
            if ($request->status == 'EXPIRE') {
                $reason->reason_for_expire = $request->reason;
                $reason->reason_for_invalid = null;
            }
            $reason->save();
        }

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")]),
            "reason" => $request->reason
        ],201);
    }

    public function driverInactiveBannedReason(Request $request,$ids)
    {
        $this->validate($request, [
            'reason'    => 'required',
        ]);
        $user = User::find($request->id);

        $reason = DriverReason::where('driver_id',$request->id)->first();

        if(empty($reason))
        {
            $reason = new DriverReason;
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        } else {
            $reason->driver_id = $request->id;
            $reason->reason = $request->reason;
            $reason->save();
        }

        //send email
        $msg="Your driver account has been Inactivated by admin. The reason for inactivation is that ".$request->reason;
        $sub = 'ZoomXoom Driver InActivation Notification';
        Notification::send($user, new EmailNotification($msg,$sub));

        return response()->json([
            "success" => true,
            "message" => "Reason inserted successfully",
            "errors" => array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")])
        ],201);
    }

}
