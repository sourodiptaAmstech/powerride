<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Request\ServiceRequestLog;
use App\Model\Request\ServiceRequestLocation;
use App\Model\ServiceType\DriverServiceType;
use App\Services\EstimatedFareService;
use App\User;


class DashboardController extends Controller
{
    
    public function index()
    {
        $serviceRequests = ServiceRequest::whereIn('request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED'])->orderBy('created_at' , 'desc')->take(15)->get();
        $totalRide = ServiceRequest::whereIn('request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])->count();

        $totalScheduleRide = ServiceRequest::whereIn('request_status', ['NOSERVICEFOUND','ACCEPTED','REACHED','STARTED','DROP','PAYMENT','RATING','COMPLETED','CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])->where('request_type','SCHEDULE')->count();
        $cancel_bypassenger = ServiceRequest::whereIn('request_status', ['CANCELBYPASSENGER'])->count();
        $cancel_bydriver = ServiceRequest::whereIn('request_status', ['CANCELBYDRIVER'])->count();
        $totalCancelRide = ServiceRequest::whereIn('request_status', ['CANCELBYSYSTEM','CANCELBYPASSENGER','CANCELBYDRIVER'])->count();
        $totalDriver = User::where('user_scope','driver-service')->count();
        //dd($totalDriver);

        // $total_reg_driver = User::where('user_scope', 'driver-service')->orWhere('user_scope', 'driver-passenger-service')->orderBy('created_at' , 'desc')->count();
        // $rides = PassengerRequest::all();
        // $totalRides = $rides->count();
        // $cancel_bydriver = PassengerRequest::where('request_status','Cancel_By_Driver')->count();
        // $cancel_bypassenger = PassengerRequest::where('request_status','Cancel_By_Passenger')->count();
        // $scheduled_rides = PassengerRequest::where('booking_type','schedule_ride')->count();
        // $total_cancel_rides = $cancel_bydriver + $cancel_bypassenger;
        // $totalEarn = null;
        // if (count($rides)>0) {
        //     foreach ($rides as $key => $ride) {
        //         if ($ride->request_status == 'Completed') {
        //             $bidRequest = BidRequest::where('passenger_request_id',$ride->passenger_request_id)->where('driver_id',$ride->driver_id)->first();
        //             if (isset($bidRequest)) {
        //                 $totalEarn = $totalEarn+$bidRequest->bid_cost;
                       
        //             }
        //         }
        //     }
        // }
        // $completed_rides = PassengerRequest::where('request_status','Completed')->orderBy('created_at' , 'desc')->take(10)->get();
        // , compact('total_reg_driver','rides','total_cancel_rides','cancel_bydriver','cancel_bypassenger','scheduled_rides','totalEarn','completed_rides')
        return view('admin.dashboard.dashboard',compact('serviceRequests','totalRide','totalScheduleRide','totalCancelRide','totalDriver','cancel_bypassenger','cancel_bydriver'));
    }


    public function dashboardRequestDetails($request_id,$param){
        $serviceRequest = ServiceRequest::find($request_id);
        $request_logs = '';
        if($serviceRequest->request_status=='NOSERVICEFOUND'){
            $request_logs = ServiceRequestLog::join('drivers_profile as dpr','dpr.user_id','=','service_request_logs.driver_id')
                ->join('service_types as st','st.id','=','service_request_logs.service_type_id')
                ->join('driver_service_type as dst','dst.user_id','=','service_request_logs.driver_id')
                ->select('dpr.first_name','dpr.last_name','service_request_logs.*','st.name','dst.model','dst.registration_no')
                ->where('request_id',$serviceRequest->request_id)->get();
        }
        
        $requests = ServiceRequestLocation::where('request_id',$serviceRequest->request_id)->get();

        $DriverServiceType = DriverServiceType::where('user_id',$serviceRequest->driver_id)->where('service_type_id',$serviceRequest->service_type_id)->first();
        
        $serviceRequest['staredFromSource_on']=$serviceRequest['started_from_source'];

        $Invoice = '';
        if ($param=='all') {
            if($serviceRequest->request_status == 'COMPLETED'){
                $EstimatedFareService=new EstimatedFareService();
                $Invoice=$EstimatedFareService->accessCalculateFinalPayable((object)$serviceRequest);
            }
        }
        foreach ($requests as $request) {
            if ($request->types=='source') {
                $serviceRequest->s_latitude = $request->latitude;
                $serviceRequest->s_longitude = $request->longitude;
                $serviceRequest->s_address = $request->address;
            }
            if ($request->types=='destination') {
                $serviceRequest->d_latitude = $request->latitude;
                $serviceRequest->d_longitude = $request->longitude;
                $serviceRequest->d_address = $request->address;
            }
        }
        
        return view('admin.dashboard.view',compact('serviceRequest','param','DriverServiceType','Invoice','request_logs'));
    }

  
    public function create()
    {
        //
    }

   
    public function store(Request $request)
    {
        //
    }

   
    public function show($id)
    {
        //
    }

    
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }

   
    public function destroy($id)
    {
        //
    }
}
