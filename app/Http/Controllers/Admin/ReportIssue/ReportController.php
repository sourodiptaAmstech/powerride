<?php

namespace App\Http\Controllers\Admin\ReportIssue;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ReportIssue\ReportIssue;
use App\Model\ReportIssue\ReportSubject;
use Exception;


class ReportController extends Controller
{
    public function driverSubjectList()
    {
    	$ReportSubject = ReportSubject::where('type','driver')->get();
    	return view('admin.reportIssue.Subject.driver-list',compact('ReportSubject'));
    }

    public function customerSubjectList()
    {
    	$ReportSubject = ReportSubject::where('type','passenger')->get();
    	return view('admin.reportIssue.Subject.customer-list',compact('ReportSubject'));
    }

    public function customerSubjectAdd(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
        ]);
        try{
            $ReportSubject= new ReportSubject;
            $ReportSubject->subject = $request->subject;
            $ReportSubject->type = 'passenger';
            $ReportSubject->save();

            return redirect()->route('admin.reportIssue.customer.subject.list')->with('flash_success','Subject Saved Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Subject Not Found');
        }
    }

    public function driverSubjectAdd(Request $request)
    {
        $this->validate($request, [
            'subject' => 'required',
        ]);
        try{
            $ReportSubject= new ReportSubject;
            $ReportSubject->subject = $request->subject;
            $ReportSubject->type = 'driver';
            $ReportSubject->save();

            return redirect()->route('admin.reportIssue.driver.subject.list')->with('flash_success','Subject Saved Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Subject Not Found');
        }
    }

    public function driverSubjectEdit($id)
    {
 		$ReportSubject= ReportSubject::find($id);
 		return view('admin.reportIssue.Subject.driver-edit',compact('ReportSubject'));
    }

    public function customerSubjectEdit($id)
    {
    	$ReportSubject= ReportSubject::find($id);
 		return view('admin.reportIssue.Subject.customer-edit',compact('ReportSubject'));
    }

    public function customerSubjectUpdate(Request $request,$id)
    {
        $this->validate($request, [
            'subject' => 'required',
        ]);
        try{
            $ReportSubject= ReportSubject::find($id);
            $ReportSubject->subject = $request->subject;
            $ReportSubject->save();

            return redirect()->route('admin.reportIssue.customer.subject.list')->with('flash_success','Subject Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Subject Not Found');
        }
    }

    public function driverSubjectUpdate(Request $request,$id)
    {
        $this->validate($request, [
            'subject' => 'required',
        ]);
        try{
            $ReportSubject= ReportSubject::find($id);
            $ReportSubject->subject = $request->subject;
            $ReportSubject->save();

            return redirect()->route('admin.reportIssue.driver.subject.list')->with('flash_success','Subject Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Subject Not Found');
        }
    }


    public function driverReportIssueList()
    {
    	$ReportIssue = ReportIssue::where('reportBy','driver')->get();
    	return view('admin.reportIssue.ViewReport.driver-report',compact('ReportIssue'));
    }

    public function customerReportIssueList()
    {
        $ReportIssue = ReportIssue::where('reportBy','passenger')->get();
    	return view('admin.reportIssue.ViewReport.customer-report',compact('ReportIssue'));
    }

    public function reportIssueDetails($issues_id,$param)
    {
    	$ReportIssue = ReportIssue::find($issues_id);
    	return view('admin.reportIssue.ViewReport.details-report',compact('ReportIssue','param'));
    }

}
