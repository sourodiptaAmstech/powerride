<?php

namespace App\Http\Controllers\Admin\Passenger;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Profiles\PassengersProfile;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use App\User;
// use Storage;
use App\Services\UsersDevices;
use App\Model\Request\ServiceRequest;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailAccountNotification;


class PassengerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.passenger.index');
    }

    public function ajaxPassenger(Request $request)
    {
        $columns = array(
            0 => 'id',
            1 => 'first_name',
            2 => 'email_id',
            3 => 'isd_code',
        );

        $totalData =  User::where('user_scope', 'passenger-service')->count();
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value'))){
            if ($order=='id') {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                    ->where('user_scope', 'passenger-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('users.'.$order,$dir)
                    ->get();
            } else {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                    ->where('user_scope', 'passenger-service')
                    ->offset($start)
                    ->limit($limit)
                    ->orderBy('pr.'.$order,$dir)
                    ->get();
            }

            $totalFiltered = User::where('user_scope', 'passenger-service')->count();
        }else{
            $search = $request->input('search.value');
            if ($order=='id') {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('users.'.$order, $dir)
                ->get();
            } else {
                $passengers = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%");
                })
                ->offset($start)
                ->limit($limit)
                ->orderBy('pr.'.$order, $dir)
                ->get();
            }

            $totalFiltered = User::join('passengers_profile as pr','users.id','=','pr.user_id')
                ->where('users.user_scope', 'passenger-service')
                ->where(function($q) use ($search){
                    $q->where('users.id', 'like', "%{$search}%")
                    ->orWhere('pr.first_name', 'like', "%{$search}%")
                    ->orWhere('pr.last_name','like',"%{$search}%")
                    ->orWhere('pr.email_id','like',"%{$search}%")
                    ->orWhere('users.username','like',"%{$search}%")
                    ->orWhere('pr.isd_code','like',"%{$search}%")
                    ->orWhere('pr.mobile_no','like',"%{$search}%");
                })
                ->count();
        }

        $data = array();

        if($passengers){
            foreach($passengers as $p){
                $nestedData['id']     = $p->id;
                $nestedData['passenger_name']   = $p->passenger_profile->first_name." ".$p->passenger_profile->last_name;
                $nestedData['email']   = $p->passenger_profile->email_id;
                $nestedData['mobile']  = $p->passenger_profile->isd_code.'-'.$p->passenger_profile->mobile_no;
                $nestedData['action'] = '<span style="line-height:33px;"><a href="passenger/'.$p->id.'/edit" class="btn btn-info">Edit</a> <a href="passenger/trip/history/'.$p->id.'" class="btn btn-info">Trip History</a> <a href="passenger/review-rating/'.$p->id.'" class="btn btn-info"> Review/Rating</a> <a href="passenger/transaction/'.$p->id.'" class="btn btn-info">Transaction</a></span>';
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    public function passengerRatingReview($id){
        $passenger = PassengersProfile::where('user_id',$id)->first();
        $serviceRequests = ServiceRequest::where('passenger_id',$id)->where('request_status','COMPLETED')->get();
        return view('admin.passenger.review-rating',compact('passenger','serviceRequests'));
    }


    public function create()
    {
        return view('admin.passenger.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'first_name'        => 'required|max:255',
            'last_name'         => 'required|max:255',
            'email'             => 'nullable|email|max:255|unique:passengers_profile,email_id',
            'picture'           => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'password'          => 'required|confirmed|min:6',
            'mobile_no'         => 'required|digits_between:5,10|unique:passengers_profile'
        ]);
        try{

            if (empty($request->mobile_no)) {
                $request->code = null;
            } else {
                $request->code = '+'.$request->code;
            }
            // creating new users
            $User = new User();
            $User->password = bcrypt(trim($request->password));
            $User->user_scope = "passenger-service";
            $User->username = $request->mobile_no;
            $User->save();

            // create the user's profile
            $PassengersProfile = new PassengersProfile();
            $PassengersProfile->user_id = $User->id;
            $PassengersProfile->email_id = $request->email;
            $PassengersProfile->first_name = $request->first_name;
            $PassengersProfile->last_name = $request->last_name;
            if(isset($request->picture) && !empty($request->picture)){
                $picture = $request->picture->store('public/passenger/profile');
                $picture = str_replace("public", "storage", $picture);
                $PassengersProfile->picture="http://104.131.120.166/powerride/public/".$picture;
            }
            $PassengersProfile->mobile_no = $request->mobile_no;
            $PassengersProfile->isd_code = $request->code;
            $PassengersProfile->dob=$request->dob;
            $PassengersProfile->gender=$request->gender;
            $PassengersProfile->isMobileverified=1;
            $PassengersProfile->save();

            //confirm it
            $request->user_id = $User->id;
            $request->device_id = "device_id";
            $request->device_token = "device_token";
            $UserDevice=new UsersDevices();
            $UserDevice->accessCreateDevices($request);

            $User->email = $request->email;
            $sub = "Login Credentials to Powerride";
            $message = "Please login to the powerride app with below login credentials.";
            $email = "Username: ".$request->code."-".$request->mobile_no;
            $password = "Password: ".$request->password;
            Notification::send($User, new EmailAccountNotification($sub,$message,$email,$password));

            if(!empty($User)){
                if($User->id>0){
                    return redirect()->route('admin.passenger.index')->with('flash_success', 'Passenger account created successfully.');
                }
            }
            return redirect()->back()->with('flash_error', 'Registration not possible!');
        }

        catch(\Illuminate\Database\QueryException  $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return redirect()->back()->with('flash_error', trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $passenger = User::find($id);
            return view('admin.passenger.edit',compact('passenger'));
        } catch (ModelNotFoundException $e) {
            return $e;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'first_name'    => 'required|max:255',
            'last_name'     => 'required|max:255',
            'email'         => 'nullable|email|unique:passengers_profile,email_id,'.$id.',user_id',
            'picture'       => 'mimes:jpeg,jpg,bmp,png|max:5242880',
            'mobile'        => 'required|digits_between:5,10|unique:passengers_profile,mobile_no,'.$id.',user_id',
        ]);
        if (empty($request->mobile)) {
            $request->code = null;
        } else {
            $request->code = '+'.(integer)$request->code;
        }
        try {
            $passenger = User::find($id);
            $passenger->username = $request->mobile;
            $passenger->save();

            $passenger_profile = PassengersProfile::where('user_id', $id)->first();
            if (isset($passenger_profile)) {
                $passenger_profile->first_name    = $request->first_name;
                $passenger_profile->last_name     = $request->last_name;
                $passenger_profile->email_id      = $request->email;
                if(isset($request->picture) && !empty($request->picture)){
                    //$Storage=Storage::delete($passenger->picture);
                    $picture = $request->picture->store('public/passenger/'.$id.'/profile');
                    $picture = str_replace("public", "storage", $picture);
                    $passenger_profile->picture="http://104.131.120.166/powerride/public/".$picture;
                }
                $passenger_profile->mobile_no = $request->mobile;
                $passenger_profile->isd_code  = $request->code;
                $passenger_profile->dob       = $request->dob;
                $passenger_profile->gender    = $request->gender;
                $passenger_profile->save();
            }

            return redirect()->route('admin.passenger.index')->with('flash_success', 'Passenger Updated Successfully');
        }
        catch (Exception $e) {
            return back()->with('flash_error', 'Passenger Not Found');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
