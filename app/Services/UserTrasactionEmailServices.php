<?php

namespace App\Services;

use App\Model\Transaction\UserTrasactionEmail;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Notification;
use App\Notifications\EmailVerificationNotification;

class UserTrasactionEmailServices
{
    private function create($data){
        $UserTrasactionEmail = new UserTrasactionEmail();
        $UserTrasactionEmail->user_id=$data->user_id;
        $UserTrasactionEmail->email_id=$data->email_id;
        $UserTrasactionEmail->isVerified=0;
        $UserTrasactionEmail->save();
        return $UserTrasactionEmail;
    }
    private function updateEmail($data){
        $UserTrasactionEmail=UserTrasactionEmail::where('user_id',$data->user_id)->first();
        $UserTrasactionEmail->email_id=$data->email_id;
        $UserTrasactionEmail->isVerified=0;
        $UserTrasactionEmail->save();
        return $UserTrasactionEmail;
    }

    private function verifyTrasactionEmail($data){
        try{
            $UserTrasactionEmail = UserTrasactionEmail::where('ute_id',$data->ute_id)->first();
            $UserTrasactionEmail->isVerified = 1;
            $UserTrasactionEmail->save();

            return ['message'=>"Thank you for verifying your email ID.","data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }


    private function get($data){
         try{
            $UserTrasactionEmail=UserTrasactionEmail::where('user_id',$data->user_id)->get()->toArray();
            if(!empty($UserTrasactionEmail))
            return ['message'=>"Email Id Found.","data"=>$UserTrasactionEmail,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>"Email Id Not Found.","data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }
    }
    public function accessTrasactionEmail($data){
        $TrasactionEmail= $this->get($data);
        if($TrasactionEmail['statusCode']===404)
        {
            $saved=$this->create($data);
            $data->user['email'] = $data->email_id;
            $urlLink="http://104.131.120.166/powerride/public/email/verification/".base64_encode($saved->ute_id);
            Notification::send( $data->user, new EmailVerificationNotification($urlLink));
           return ['message'=>[],"data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200,"status"=>true];
        }
        elseif($TrasactionEmail['statusCode']===200){
            if($TrasactionEmail['data'][0]['isVerified']==0){
                $saved=$this->updateEmail($data);
                $data->user['email'] = $data->email_id;
                $urlLink="http://104.131.120.166/powerride/public/email/verification/".base64_encode($saved->ute_id);
                Notification::send( $data->user, new EmailVerificationNotification($urlLink));
                return ['message'=>[],"data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200,"status"=>true];
            } else {
                return ['message'=>[],"data"=>(object)[],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200,"status"=>false];
            }

        }
    }

    public function checkVerifyEmail($data){
        $TrasactionEmail= $this->get($data);
        $return= ['message'=>trans("api.SYSTEM_MESSAGE.EMAIL_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>404];
        if($TrasactionEmail['statusCode']===404)
        {
            $return= ['message'=>trans("api.SYSTEM_MESSAGE.EMAIL_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>404];
        }
        elseif($TrasactionEmail['statusCode']===200){
            if($TrasactionEmail['data'][0]['isVerified']==0){
                $return= ['message'=>trans("api.SYSTEM_MESSAGE.EMAIL_NOT_VERIFIED"),"data"=>$TrasactionEmail['data'][0]['email_id'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>403];
            }
            else{
                $return= ['message'=>trans("api.SYSTEM_MESSAGE.EMAIL_VERIFIED"),"data"=>$TrasactionEmail['data'][0]['email_id'],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
            }
        }

        return $return;


    }

    public function accessVerifyTrasactionEmail($data){
        return $this->verifyTrasactionEmail($data);
    }
}


