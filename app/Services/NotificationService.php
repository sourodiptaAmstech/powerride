<?php

namespace App\Services;

use App\User;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\DB;
use App\Model\Notification\Notification;


class NotificationService
{
    private function createNotification($data){
       $Notification=new Notification();
       $Notification->user_id=$data->user_id;
       $Notification->title=$data->title;
       $Notification->text=$data->text;
       $Notification->request_id=$data->request_id;
       $Notification->save();
       return $Notification;
    }

    private function getNotification($user_id,$timeZone){
       // echo $timeZone."===================";
        $timeZone=explode(":",trim(trim(preg_replace('/\s+/', '', str_replace("GMT","",trim(preg_replace('/\s+/', '', $timeZone))))),''));
        //print_r($timeZone);

        $messageList=Notification::where("user_id",$user_id)
        ->select('title','text','request_id','created_at') ->paginate(10)->toArray();

        return $messageList;
    }

    public function accessCreateNotification($data){
        return $this->createNotification($data);
    }
    public function accessGetNotification($user_id,$timeZone){
        return $this->getNotification($user_id,$timeZone);
    }
}
