<?php

namespace App\Services;

use App\Model\Transaction\TransactionLog;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Model\Request\ServiceRequest;


class TransactionLogService
{
    public function createCreditNoteForRide($data){
        $TransactionLog=TransactionLog::create($data);
        return $TransactionLog;
    }

    public function getCreditNoteForRide($data){
        try{
            $TransactionLog = TransactionLog::where("request_id",$data->request_id)->firstOrFail();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id,"status"=>$TransactionLog->status,"TransactionLog"=>$TransactionLog],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }

    public function getCreditNoteForRideForTripHistory($data){
        try{
            $TransactionLog = TransactionLog::select("cost","distance_miles","currency","promo_code_value","promo_code")->where("request_id",$data->request_id)->firstOrFail();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id,"status"=>$TransactionLog->status,"TransactionLog"=>$TransactionLog],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
    public function updateCreditNoteStatusForRide($data){
        try{
            $TransactionLog = TransactionLog::where("request_id",$data->request_id)->firstOrFail();
            $TransactionLog->status="COMPLETED";
            $TransactionLog->save();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }

    }
    public function updateTransactionDetails($data){
        try{
            //

            $TransactionLog = TransactionLog::where("request_id",$data->request_id)->firstOrFail();
            $TransactionLog->status="COMPLETED";
            $TransactionLog->payment_gateway_transaction_id=$data->payment_gateway_transaction_id;
            $TransactionLog->payment_gateway_charge=$data->payment_gateway_charge;
            $TransactionLog->save();

            return ['message'=>"OK","data"=>["transaction_log_id"=>$TransactionLog->transaction_log_id],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function transactionHistory($data)
    {
        $serviceRequest = ServiceRequest::join('transaction_log', 'transaction_log.request_id','=','service_requests.request_id')
            ->where('transaction_log.status','COMPLETED')
            ->where('service_requests.driver_id','=',$data->driver_id)->select('service_requests.driver_id','transaction_log.*')->get();
        $cash_cost=0;
        $card_cost=0;
        $cash_adminCommission=0;
        $card_adminCommission=0;
        $cash_cost_y=0;
        $card_cost_y=0;
        $cash_adminCommission_y=0;
        $card_adminCommission_y=0;
        $cash_cost_n=0;
        $card_cost_n=0;
        $cash_adminCommission_n=0;
        $card_adminCommission_n=0;
        foreach ($serviceRequest as $service) {
            if ($service->payment_method=='CASH') {
                if ($service->is_paid=='Y') {
                    $cash_cost_y = $cash_cost_y+$service->cost;
                    $cash_adminCommission_y=$cash_adminCommission_y+$service->payment_gateway_charge;
                }
                if ($service->is_paid=='N') {
                    $cash_cost_n = $cash_cost_n+$service->cost;
                    $cash_adminCommission_n=$cash_adminCommission_n+$service->payment_gateway_charge;
                }
            }
            if ($service->payment_method=='CARD') {
                if ($service->is_paid=='Y') {
                    $card_cost_y = $card_cost_y+$service->cost;
                    $card_adminCommission_y=$card_adminCommission_y+$service->payment_gateway_charge;
                }
                if ($service->is_paid=='N') {
                    $card_cost_n = $card_cost_n+$service->cost;
                    $card_adminCommission_n=$card_adminCommission_n+$service->payment_gateway_charge;
                }
            }
        }
        // dd($cash_adminCommission_y);
        // dd($card_adminCommission_y);

        return $serviceRequest;
    }

    public function getCancelFee($data){
        try{
            $getCancelFee = ServiceRequest::join('transaction_log', 'transaction_log.request_id','=','service_requests.request_id')
            ->where('transaction_log.status','PENDING')
            ->where('transaction_log.transaction_type','CANCELBYPASSENGERCHARGE')
            ->where('service_requests.passenger_id','=',$data->user_id)->sum('transaction_log.cost');
            return ['message'=>"OK","data"=>$getCancelFee,"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    public function mapCancelPaymentWithRide($data){
        try{
            $serviceRequest = ServiceRequest::where("request_id",$data->request_id)->first();
            $getCancelFee = ServiceRequest::join('transaction_log', 'transaction_log.request_id','=','service_requests.request_id')
            ->where('transaction_log.status','PENDING')
            ->where('transaction_log.transaction_type','CANCELBYPASSENGERCHARGE')
            ->where('service_requests.passenger_id','=',$serviceRequest->passenger_id)->sum('transaction_log.cost');
            if($getCancelFee>0){
                   $mapCancelPaymentWithRide = ServiceRequest::join('transaction_log', 'transaction_log.request_id','=','service_requests.request_id')
            ->where('transaction_log.status','PENDING')
            ->where('transaction_log.transaction_type','CANCELBYPASSENGERCHARGE')
            ->where('service_requests.passenger_id','=',$serviceRequest->passenger_id)->select('transaction_log.*')->get()->toArray();
            foreach($mapCancelPaymentWithRide as $key=>$val){
                $updateCancle = TransactionLog::where("request_id",$val['request_id'])->first();
                $updateCancle->payment_gateway_transaction_id=$data->request_id;
                $updateCancle->payment_method=$serviceRequest->payment_method;
                $updateCancle->save();
            }
        }
        return ['message'=>"OK","data"=>$getCancelFee,"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function mapedCancelPaymentWithRide($data){
        try{
            $getCancelFee=TransactionLog::where("payment_gateway_transaction_id",$data->request_id)->where('status','PENDING')->where('transaction_type','CANCELBYPASSENGERCHARGE')->sum('cost');
            return ['message'=>"OK","data"=>$getCancelFee,"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    public function makeCancelPayment($data){
        try{
            $makeCancelPayment=TransactionLog::where("payment_gateway_transaction_id",$data->request_id)->where('status','PENDING')->where('transaction_type','CANCELBYPASSENGERCHARGE')->get()->toArray();

            foreach($makeCancelPayment as $key=>$val){
                $updateCancle = TransactionLog::where("request_id",$val['request_id'])->first();
                $updateCancle->status="COMPLETED";
             //   $updateCancle->payment_method=$serviceRequest->payment_method;
                $updateCancle->save();
                $UpdatePayment=ServiceRequest::where("request_id",$val['request_id'])->first();
                $UpdatePayment->payment_status="COMPLETED";
                $UpdatePayment->save();
            }

            return ['message'=>"OK","data"=>[],"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }
    public function getPaidCancelAmount($data){
        try{
            $cancelAmount=TransactionLog::where("payment_gateway_transaction_id",$data->request_id)->sum('cost');
            return ['message'=>"OK","data"=>$cancelAmount,"errors"=>array("exception"=>["Resoures Created"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"System error!","data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }



    public function accessTransactionHistory($data)
    {
        return $this->transactionHistory($data);
    }

}



