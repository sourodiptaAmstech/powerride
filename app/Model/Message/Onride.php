<?php

namespace App\Model\Message;

use Illuminate\Database\Eloquent\Model;

class Onride extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'messages_onride';
    protected $primaryKey = 'message_id';


}
