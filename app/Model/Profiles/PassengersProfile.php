<?php

namespace App\Model\Profiles;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class PassengersProfile extends Model
{
    use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'passengers_profile';
    protected $primaryKey = 'passenger_id';

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}
