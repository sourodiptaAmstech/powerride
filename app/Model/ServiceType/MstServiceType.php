<?php

namespace App\Model\ServiceType;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MstServiceType extends Model
{
    use SoftDeletes;
   // use Notifiable;
    
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_types';
    protected $primaryKey = 'id';
}
