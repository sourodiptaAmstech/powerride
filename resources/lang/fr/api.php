<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// framework messages
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'notAuthorize'=>'Your not authorize to access this page',
    'SYSTEM_MESSAGE'=>[

        'INVALID_LANGUAGE_SELECTION'=>'Invalid Language Selection',
        'VALIDATION_FAILED'=>'Request Validation Failed',
        'SOMETHING_WENT_WRONG'=>'Something went wrong',
        'HTTP_BAD_REQUEST'=>'Http Response Exception: Bad Request',
        'DATABASE_EXCEPTION'=>'DataBase Exception',
        'WORKING_AS_EXPECTED'=>'is working as expected',
        'SERVICE_LIST'=>'Service list',
        'EMAIL_NOT_FOUND'=>'Email Id Not Found.',
        'EMAIL_NOT_VERIFIED'=>'Email id not verified.',
        'EMAIL_VERIFIED'=>'Email id verified.',
        'LOGGED_IN'=>'Logged in!',
        'DOCUMENT_UPDATED'=>'Document updated successfully',
        'DOCUMENT_LIST'=>'Document list',
        'MESSAGE_SENT'=>'Message sent!',
        'OLD_PASSWORD_NOT_MATCH'=>'Your old password does not match with the password you provided. Please try again',
        'NEW_PASSWORD_SAME_AS_OLD'=>'New Password cannot be same as your old password. Please choose a different password',
        'PROVIDE_VALID_PHONE_NUMBER'=>'Please profide valid phone number.',
        'PHONE_NUMBER_VERIFED'=>'Thank you for verifing your phone no with us!',
        'PHONE_NUMBER_NOT_VERIFED'=>'Please verify your phone number.',
        'REGISTRATION_FAILED'=>'Registration not possible. Contact Administrator.',
        'WAIT_FOR_CUSTOMER'=>'Please wait for the customer',
        'REQUEST_DECLINE'=>'You have decline, a request',
        'REQUEST_ACCEPTED'=>'Thank you for accepting the request, please proceed towards the pickup location.',
        'PROCEED_TO_DESTINATION'=>'Please proceed towards the destination.',
        'PAYMENT_COLLECTED'=>'Thank you for collecting the payment',
        'CALCULATED_PAYABLE'=>'Please wait a while, as we calculate the payable fare',
        'RATING_CUSTOMER'=>'Thank you, for rating the customer.',
        "ON_RIDE"=>"It's seems your on ride already, you cancel ride request cannot be processed."
    ],
    'NOTIFICATION'=>[
        'TITLE'=>[
            'RIDE_REQUEST'=>'Ride Request',
            'DRIVER_CHAT'=>'Driver Message',
            'PASSENGER_CHAT'=>'Passenger Message',
            'ADMIN_CHAT'=>'Admin Message',
            'RIDE_ACCEPTED'=>'Ride Accepted',
            'RIDE_REACHED'=>'Your Driver/Ride has arrived',
            'RIDE_COMPLETED'=>'Ride Completed',
            'REQUEST_CANCLED'=>'Ride Request Cancelled',
            'SCHEDULED_RIDE_CANCEL'=>'Scheduled Ride Cancelled',

        ],
        'MESSAGE'=>[
            'RIDE_REQUEST'=>'You have a new ride request.',
            'RIDE_ACCEPTED'=>'Driver has been accepted your ride request. Soon be reached in your pickup location.',
            'RIDE_REACHED'=>'Driver has arrived at your pickup location.',
            'RIDE_COMPLETED'=>'Thank you for choosing POWER RIDE. Please call again.',
            'RIDE_COMPLETED_DRIVER'=>'Thank you for choosing POWER RIDE. Please call again.',
            'REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER'=>'Ride request is cancelled by the driver.',
            'REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER'=>'Ride request has been cancelled by the passenger.',
            'REQUEST_CANCEL_BY_DRIVER'=>'Ride request cancelled by you. The cancellation charge of :cancelAmount :currency will be deducted from your wallet.',
            'REQUEST_CANCEL_BY_PASSENGER'=>'Your request has been cancelled.',
            'REQUEST_CANCEL_BY_PASSENGER_WITH_FEE'=>'Your request has been cancelled and :cancelAmount :currency will be charged from your next ride as the cancellation fee',
            'SCHEDULED_RIDE_CANCEL'=>'Your scheduled ride which is due in next 10 minutes, has been cancelled by us as you are already on a ride.',
            ]
        ],

      'MESSAGE'=>[

      ]


    ];
