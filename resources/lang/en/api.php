<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
// framework messages
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'notAuthorize'=>'Your not authorize to access this page',
    'SYSTEM_MESSAGE'=>[
        'LOGGED_IN'=>'Logged in!',
        'DOCUMENT_UPDATED'=>'Document updated successfully',
        'DOCUMENT_LIST'=>'Document list',
        'MESSAGE_SENT'=>'Message sent!',
        'OLD_PASSWORD_NOT_MATCH'=>'Your old password does not match with the password you provided. Please try again',
        'NEW_PASSWORD_SAME_AS_OLD'=>'New Password cannot be same as your old password. Please choose a different password',
        'PROVIDE_VALID_PHONE_NUMBER'=>'Please profide valid phone number.',
        'PHONE_NUMBER_VERIFED'=>'Thank you for verifing your phone no with us!',
        'PHONE_NUMBER_NOT_VERIFED'=>'Please verify your phone number.',
        'REGISTRATION_FAILED'=>'Registration not possible. Contact Administrator.',
        'WAIT_FOR_CUSTOMER'=>'Please wait for the customer',
        'REQUEST_DECLINE'=>'You have decline, a request',
        'REQUEST_ACCEPTED'=>'Thank you for accepting the request, please proceed towards the pickup location.',
        'PROCEED_TO_DESTINATION'=>'Please proceed towards the destination.',
        'PAYMENT_COLLECTED'=>'Thank you for collecting the payment',
        'CALCULATED_PAYABLE'=>'Please wait a while, as we calculate the payable fare',
        'RATING_CUSTOMER'=>'Thank you, for rating the customer.',
        "ON_RIDE"=>"It's seems your on ride already, you cancel ride request cannot be processed.",
        'INVALID_LANGUAGE_SELECTION'=>'Invalid Language Selection',
        'VALIDATION_FAILED'=>'Request Validation Failed',
        'SOMETHING_WENT_WRONG'=>'Something went wrong',
        'HTTP_BAD_REQUEST'=>'Http Response Exception: Bad Request',
        'DATABASE_EXCEPTION'=>'DataBase Exception',
        'WORKING_AS_EXPECTED'=>'is working as expected',
        'SERVICE_LIST'=>'Service list',
        'EMAIL_NOT_FOUND'=>'Email Id Not Found.',
        'EMAIL_NOT_VERIFIED'=>'Email id not verified.',
        'EMAIL_FOUND'=>'Email Id Found.',
        'EMAIL_VERIFIED'=>'Email id verified.',
        '404_ERROR'=>'No Content',
        'INTERNAL_SERVER_ERROR'=>'Internal Server Error',
        'MAKE_FOUND'=>'List of car makers',
        'MAKE_NOT_FOUND'=>'Car makers not found.',
        'MODEL_FOUND'=>'List of car models',
        'MODEL_NOT_FOUND'=>'Car model not found.',
        'MODEL_YEAR_FOUND'=>'List of car model years',
        'MODEL_YEAR_NOT_FOUND'=>'Year not found.',
        'verifying_email_ID'=>'Thank you for verifying your email ID',
        'Everything_OK'=>'Everything OK',
        'Not_Found_Exception'=>'Model Not Found Exception',
        'Password_successfully_Changed'=>'Your account password is successfully changed',
        'Not_Registered'=>'You are not registered with us!',
        'Exception_From_Twilo'=>'Exception From Twilo',
        'OTP_Sent'=>'OTP sent successfully!',
        'System_Error'=>'System error!',
        'Resoures_Created'=>'Resoures Created',
        'Promo_Code_Not_Exit'=>'Promo code does not exit',
        'Promo_Code_Not_Exit_Redeemed.'=>'The promo code you entered has already been redeemed.',
        'Promo_Code_Added'=>'Promo code added',
        'Promo_Code_Available'=>'Promo code available',
        'List_Promo_Codes'=>'List of Promo codes',
        'Card_List'=>'Card List',
        'No_Card_Found'=>'No card found. Please add card.',
        'Cannot_Fetch_Card'=>'Cannot able to fetch your card. Please try later',
        'Card_Added'=>'Card Added',
        'Cannot_Add_Card!'=>'Cannot able to add card!',
        'Default_Card_Updated'=>'Default card updated',
        'Cannot_Update_Card'=>'Cannot able to update your card. Please try later',
        'No_Card_Found.'=>'No card found.',
        'Card deleted'=>'Card deleted',
        'cannot_delete_card'=>'Cannot able to delete your card. Please try later',
        'cannot_make_payment_from_card'=>'Cannot able to make payment from your card. Please try later',
        'profile_updated'=>'Your profile is successfully updated',
        'profile_cannot_updated!'=>'Your profile cannot be updated!',
        'otp_for_mobile_number_verification'=>'Your one time password for your mobile number verification is :OTP',
        'thank_you_verifying_mobile_number'=>'Thank you for verifying your mobile number',
        'sorry_cannot_verify_mobile_number'=>'Sorry cannot verify your mobile number',
        'thank_you_uploading_profile_image!'=>'Thank you for uploading your profile image!',
        'cars_available_your_location'=>'Cars are available for your location at that given moment.',
        'no_cars_available_your_location'=>'No cars are available for your location at that given moment.',
        'services_added'=>'Services Added',
        'mobile_number_password_is_incorrect'=>'The mobile number or password you entered is incorrect',
        'incorrect_method_authentication'=>'Incorrect method of authentication.',
        'logout_successfully'=>'You have logout successfully.',
        'user_not_found'=>'User not found',
        'you_registered_using_social_platform'=>'you have registered using social platform. Reset your password from the respective social platform.',
        'no_mobile_number_found'=>'No mobile number found',

        'otp_to_reset_password'=>'Your one time password to reset your POWER RIDE account is :OTP',

        'email_text_verification'=>'Please verify your account by clicking on the following verification link/button: ',
        'email_text_application_thanks'=>'Thank you for using our application!',
        'email_otp_for_resetpassword'=>'You are receiving this email because we received a password reset request for your account. Your OTP is',
        'email_otp_for_resetpassword_message'=>'If you did not request a password reset, no further action is required.',
        'no_driver_available'=>'Sorry no driver is available at this moment. Please try after some time.',
        'connecting_with_drivers'=>'Connecting with near by drivers.'
    ],
    'NOTIFICATION'=>[
        'TITLE'=>[
            'RIDE_REQUEST'=>'Ride Request',
            'DRIVER_CHAT'=>'Driver Message',
            'PASSENGER_CHAT'=>'Passenger Message',
            'ADMIN_CHAT'=>'Admin Message',
            'RIDE_ACCEPTED'=>'Ride Accepted',
            'RIDE_REACHED'=>'Your Driver/Ride has arrived',
            'RIDE_COMPLETED'=>'Ride Completed',
            'REQUEST_CANCLED'=>'Ride Request Cancelled',
            'SCHEDULED_RIDE_CANCEL'=>'Scheduled Ride Cancelled',

        ],
        'MESSAGE'=>[
            'RIDE_REQUEST'=>'You have a new ride request.',
            'RIDE_ACCEPTED'=>'Driver has accepted your ride request and will reach to your pick up location soon',
            'RIDE_REACHED'=>'Driver has arrived at your pickup location.',

            'RIDE_COMPLETED'=>'Thanks for choosing Power Ride. Looking to serve you in future as well.',

            'REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER'=>'Ride request is cancelled by the driver.',
            'REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER'=>'Ride request has been cancelled by the passenger.',
            'REQUEST_CANCEL_BY_DRIVER'=>'Ride request cancelled by you. The cancellation charge of :cancelAmount :currency will be deducted from your wallet.',
            'REQUEST_CANCEL_BY_PASSENGER'=>'Your request had been cancelled.',
            'REQUEST_CANCEL_BY_PASSENGER_WITH_FEE'=>'Your request has been cancelled and :cancelAmount :currency will be charged from your next ride as the cancellation fee',
            'SCHEDULED_RIDE_CANCEL'=>'Your scheduled ride which is due in next 10 minutes, has been cancelled by us as you are already on a ride.',
            ]
        ],

      'MESSAGE'=>[

      ]


    ];
