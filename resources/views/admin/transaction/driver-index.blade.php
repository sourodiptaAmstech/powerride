@extends('admin.layout.base')
@section('title', 'Transaction')
@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
        <h5 class="mb-1">Transaction</h5>
        <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
        {{-- data-toggle="modal" data-target="#myModal" --}}
        <a href="javascript:void(0)" class="btn btn-primary pull-left" style="margin-right: 10px;" id="payout"> Payout</a>
        <table class="table table-striped table-bordered dataTable" id="table-transac">
            <thead>
                <tr>
                    <th></th>
                    <th>Request Number</th>
                    <th>Booking Date</th>
                    <th>Payment Method</th>
                    <th>Tax</th>
                    <th>Insurance</th>
                    <th>Commission</th>
                    <th>Driver Cancel Amount</th>
                    <th>Passenger Cancel Amount</th>
                    <th>Total Amount</th>
                    {{-- <th>Payable Amount</th> --}}
                </tr>
            </thead>
            <tbody>
            @foreach($serviceRequest as $index => $service)
                <tr>
                    <td class="check4">
                      {{-- {{$service->request_id}} --}}
                      @if ($service->is_paid=='N')
                      <input type="checkbox" id="check{{$service->request_id}}" key="{{$service->request_id}}" name="check" class="check" value="{{$service->request_id}}">
                      @endif
                      <span class="nr" style="display: none;">{{$service->request_id}}</span>
                      <input type="hidden" id="currency" name="currency" value="{{$service->currency}}">
                    </td>
                    <td class="nr">{{$service->request_no}}</td>
                    <td class="nr">{{date('Y-m-d h:i A',strtotime($service->created_at)) }}</td>
                    <td class="nr">{{$service->payment_method}}</td>
                    <td class="nr">{{$service->tax}}</td>
                    <td class="nr">{{$service->ride_insurance}}</td>
                    <td class="nr">{{$service->commission}}</td>
                    <td class="nr">{{$service->driverCancelAmount}}</td>
                    <td class="nr">{{$service->passengerCancelAmount}}</td>
                    <td class="nr">{{$service->cost}}</td>
                    {{-- <td>{{$service->cost}}</td> --}}
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                  <th></th>
                  <th>Request Number</th>
                  <th>Booking Date</th>
                  <th>Payment Method</th>
                  <th>Tax</th>
                  <th>Insurance</th>
                  <th>Commission</th>
                  <th>Driver Cancel Amount</th>
                  <th>Passenger Cancel Amount</th>
                  <th>Total Amount</th>
                  {{-- <th>Payable Amount</th> --}}
                </tr>
            </tfoot>
        </table>
		</div>
    </div>
</div>
<!-- The Modal -->
<div class="modal fade" id="transactionModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
    
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">Payable Transaction</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <form id="payoutRequest">
        <!-- Modal body -->
        <div class="modal-body">
          <div class="alert alert-danger loc-danger" role="alert" style="display:none">
              <p class="loc-error" style="color: red;"></p>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">A. Total ride amount received</label>
            <label for="name" class="col-xs-6 col-form-label" id="total_ride_amount"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">B. Total cash in hand(driver)</label>
            <label for="name" class="col-xs-6 col-form-label" id="total_cash"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">C. Total insurance amount</label>
            <label for="name" class="col-xs-6 col-form-label" id="insurance"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">D. Total tax amount</label>
            <label for="name" class="col-xs-6 col-form-label" id="tax"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">E. Total commission</label>
            <label for="name" class="col-xs-6 col-form-label" id="commission"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">F. Total passenger cancel charge</label>
            <label for="name" class="col-xs-6 col-form-label" id="passenger_charge"></label>
          </div>
          <div class="form-group row">
            <label for="name" class="col-xs-6 col-form-label">G. Total driver cancel charge</label>
            <label for="name" class="col-xs-6 col-form-label" id="driver_charge"></label>
          </div>
          <div class="form-group row">
              <label for="name" class="col-xs-6 col-form-label">Total payable amount<p>(A-B-C-D-E-F-G)</p></label>
              <label for=""></label>
              <label for="name" class="col-xs-6 col-form-label" id="total_payable"></label>
          </div>
          <div class="form-group row">
              <label for="transaction_id" class="col-xs-3 col-form-label">Transaction ID</label>
              <div class="col-xs-9">
                  <input class="form-control" type="text" value="{{ old('transaction_id') }}" name="transaction_id" id="transaction_id" placeholder="Transaction ID" required1>
                  <span class="text-danger" id="transac"></span>
              </div>
          </div>
          <input type="hidden" name="cost" id="cost">
          <input type="hidden" name="driver_id" id="driver_id" value="{{$id}}">
          <input type="hidden" name="request_id" id="request_id">
          <div class="form-group row">
              <label for="comments" class="col-xs-3 col-form-label">Comments</label>
              <div class="col-xs-9">
                <textarea name="comments" id="comments" rows="2" class="form-control" placeholder="Comments" required1></textarea>
                <span class="text-danger" id="coment"></span>
              </div>
          </div>
        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Pay</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
      
    </div>
  </div>
</div>
<style>
  .bootbox-body{
    color: red;
  }
</style>
<div id="bootboxModal" class="modal fade" data-backdrop="static">
  <div class="modal-dialog">
      <div class="modal-content">
          <div class="modal-body bootboxBody">
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-primary bootboxBtn">OK</button>
          </div>
      </div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  $('#table-transac').DataTable( {
      responsive: true,
      dom: 'Bfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
      "columnDefs": [
        { "orderable": false, "targets": [0,2,3,4,5,6,7,8,9] },
      ]
  });
  

  var convertedIntoArray = [];
  $(document).on('click','.check',function(){
    if($(this).is(":checked") == true){
      $(this).closest('tr').each(function() {
        var rowDataArray = [];
        var actualData = $(this).find('.nr');
        if (actualData.length > 0) {
          actualData.each(function() {
              rowDataArray.push($(this).text());
          });
          convertedIntoArray.push(rowDataArray);
        }
      });
      console.log(convertedIntoArray)
    } else if($(this).is(":checked") == false){
      
      var rowDataArray1 = [];
      $(this).closest('tr').each(function() {
        var actualData1 = $(this).find('.nr');
        if (actualData1.length > 0) {
          actualData1.each(function() {
              rowDataArray1.push($(this).text());
          });
        }
      });

      for( var i = 0; i < convertedIntoArray.length; i++){
        if (convertedIntoArray[i][0] == rowDataArray1[0]) {
          convertedIntoArray.splice(i, 1);
        }
      }
      console.log(convertedIntoArray)
    }
  });
  
  $(document).ready(function(){
    $('#payout').click(function(){
          if($('.check').is(":checked") == true){
              console.log(convertedIntoArray);
              var request_id_arr = []
              var Tax = 0
              var Insurance = 0
              var Commission = 0
              var driver_charge = 0
              var pass_charge = 0
              var Total_ride_amount = 0
              var Total_cash = 0 
              $.each(convertedIntoArray, function(key, value) {
                request_id_arr.push(value[0]);
                Tax += parseFloat(value[4]);
                Insurance += parseFloat(value[5]);
                if((value[6])!=''){
                  Commission += parseFloat(value[6]);
                }
                driver_charge += parseFloat(value[7]);
                pass_charge += parseFloat(value[8]);
                Total_ride_amount += parseFloat(value[9]);
                if(value[3]=='CASH'){
                  Total_cash += parseFloat(value[9]);
                }
              });
              var currency = $('#currency').val();
              var Total_payable = Total_ride_amount - Total_cash - Tax - Insurance - Commission - driver_charge - pass_charge;
              $('#total_ride_amount').text(currency+' '+Total_ride_amount.toFixed(2));
              $('#total_cash').text(currency+' '+Total_cash.toFixed(2));
              $('#insurance').text(currency+' '+Insurance.toFixed(2));
              $('#tax').text(currency+' '+Tax.toFixed(2));
              $('#commission').text(currency+' '+Commission.toFixed(2));
              $('#passenger_charge').text(currency+' '+pass_charge.toFixed(2));
              $('#driver_charge').text(currency+' '+driver_charge.toFixed(2));
              $('#total_payable').text(currency+' '+Total_payable.toFixed(2));
              $('#cost').val(Total_payable.toFixed(2));
              $('#request_id').val(request_id_arr);
              $('#transactionModal').modal('show');
          } else {
            bootbox.alert("Please select any one of the below checkbox.");
          }

      // });
    });


    $('#payoutRequest').on('submit', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var cost = $('#cost').val();
        var comments = $('#comments').val();
        var transaction_id = $('#transaction_id').val();
        var driver_id = $('#driver_id').val();
        var request_id = $('#request_id').val();
        $.ajax({
            url: "{{ route('admin.driver.transaction') }}",
            method:"POST",
            data:{ comments: comments, transaction_id: transaction_id, driver_id: driver_id, request_id: request_id, cost: cost },
            success: function(response){
              // bootbox.alert(response.message);
              $('#transactionModal').modal('hide');
              $('#bootboxModal').modal('show');
              $('.bootboxBody').text(response.message);
              $('button.bootboxBtn').on('click', function(){
                  $("#bootboxModal").modal('hide');
                  location.reload();
              });
            },
            error: function(response){
                if (response.status == 422){
                    var responseMsg = $.parseJSON(response.responseText);
                    if (responseMsg.errors.hasOwnProperty('comments')) {
                        $('#coment').html(responseMsg.errors.comments).promise().done(function(){
                            $('#comments').addClass('has-error');
                        });
                    }
                    if (responseMsg.errors.hasOwnProperty('transaction_id')) {
                        $('#transac').html(responseMsg.errors.transaction_id).promise().done(function(){
                            $('#transaction_id').addClass('has-error');
                        });
                    }
                }
            }
        });
    });


  });
</script>
@endsection