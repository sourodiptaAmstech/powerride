@extends('admin.layout.base')
@section('title', 'Admin Earning')
@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
        <h5 class="mb-1">Admin Earning</h5>
        <table class="table table-striped table-bordered dataTable table-responsive" id="table-transac">
            <thead>
                <tr>
                    <th style="width: 50px !important;">Request Number</th>
                    <th style="width: 50px !important;">Request Status</th>
                    <th style="width: 50px !important;">Total Fare (A+B+C+D+E)</th>
                    <th style="width: 50px !important;">Tax(A)</th>
                    <th style="width: 50px !important;">Insurance(B)</th>
                    <th style="width: 50px !important;">Commission(C)</th>
                    <th style="width: 50px !important;">Driver Cancel Amount(D)</th>
                    <th style="width: 50px !important;">Passenger Cancel Amount(E)</th>
                    <th style="width: 50px !important;">Total Earning (C+D+E)</th>
                </tr>
            </thead>
            <tbody>
            @foreach($serviceRequest as $index => $service)
                <tr>
                    <td class="nr">{{$service->request_no}}</td>
                    <td class="nr">{{$service->request_status}}</td>
                    <td class="nr">{{$service->tax+$service->ride_insurance+$service->commission + $service->driverCancelAmount + $service->passengerCancelAmount}}</td>
                    <td class="nr">{{$service->tax}}</td>
                    <td class="nr">{{$service->ride_insurance}}</td>
                    <td class="nr">{{$service->commission}}</td>
                    <td class="nr">{{$service->driverCancelAmount}}</td>
                    <td class="nr">{{$service->passengerCancelAmount}}</td>
                    <td class="nr">{{$service->commission + $service->driverCancelAmount + $service->passengerCancelAmount}}</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
                <tr>
                  <th style="width: 50px !important;">Request Number</th>
                  <th style="width: 50px !important;">Request Status</th>
                  <th style="width: 50px !important;">Total Fare (A+B+C+D+E)</th>
                  <th style="width: 50px !important;">Tax(A)</th>
                  <th style="width: 50px !important;">Insurance(B)</th>
                  <th style="width: 50px !important;">Commission(C)</th>
                  <th style="width: 50px !important;">Driver Cancel Amount(D)</th>
                  <th style="width: 50px !important;">Passenger Cancel Amount(E)</th>
                  <th style="width: 50px !important;">Total Earning (C+D+E)</th>
                </tr>
            </tfoot>
        </table>
		</div>
    </div>
</div>
@endsection
@section('scripts')
<script>
  $('#table-transac').DataTable({
      responsive: true,
      dom: 'Bfrtip',
      buttons: [
          'copyHtml5',
          'excelHtml5',
          'csvHtml5',
          'pdfHtml5'
      ],
  });
</script>
@endsection