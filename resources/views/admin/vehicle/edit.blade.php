@extends('admin.layout.base')

@section('title', 'Update Vehicle ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.vehicle.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Update Vehicle</h5>

            <form class="form-horizontal" action="{{route('admin.vehicle.update', $vehicle->id)}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	<input type="hidden" name="_method" value="PATCH">
				<div class="form-group row">
					<label for="year" class="col-xs-12 col-form-label">Year</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $vehicle->year }}" name="year" required id="year" placeholder="Year">
					</div>
				</div>

				<div class="form-group row">
					<label for="make" class="col-xs-12 col-form-label">Make</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $vehicle->make }}" name="make" required id="make" placeholder="Make">
					</div>
				</div>

				<div class="form-group row">
					<label for="model" class="col-xs-12 col-form-label">Model</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" value="{{ $vehicle->model }}" name="model" required id="model" placeholder="Model">
					</div>
				</div>

				<div class="form-group row">
					<label for="zipcode" class="col-xs-12 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Update Vehicle</button>
						<a href="{{route('admin.vehicle.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>

@endsection
