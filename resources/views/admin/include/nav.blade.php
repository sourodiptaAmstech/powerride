<style>
.with-sub a ~ ul li a{
	position:relative !important;
}
.with-sub a ~ ul li a:after {
    content: "";
    width: 10px;
    height: 10px;
    background: #989898;
    z-index: 999;
    position: absolute;
    border-radius: 50%;
    left: 30px;
    top: 12px;
}
.site-sidebar .sidebar-menu > li.with-sub > a .s-caret{
	transform: rotate(0deg);
}
.site-sidebar .sidebar-menu > li.with-sub.active > a .s-caret{
	transform: rotate(180deg);
}
</style>
<div class="site-sidebar">
	<div class="custom-scroll custom-scroll-light">
		<ul class="sidebar-menu">
			<li class="menu-title">Admin Dashboard</li>
			<li>
				<a href="{{ route('admin.dashboard.index') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fas fa-tachometer-alt"></i></span>
					<span class="s-text">Dashboard</span>
				</a>
			</li>
			
			<li class="menu-title">Members</li>
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="fas fa-user-alt"></i></span>
					<span class="s-text">Passengers</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.passenger.index') }}">List Passengers</a></li>
					<li><a href="{{ route('admin.passenger.create') }}">Add New Passenger</a></li>
				</ul>
			</li>

			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="fas fa-user-alt"></i></span>
					<span class="s-text">Drivers</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.driver.index') }}">Registered Drivers</a></li>
					<li><a href="{{ route('admin.driver.create') }}">Add New Driver</a></li>
				</ul>
			</li>

			<li class="menu-title">General</li>
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="fa fa-bar-chart" aria-hidden="true"></i></span>
					<span class="s-text">Report</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.request.all') }}">All Request</a></li>
					<li><a href="{{ route('admin.request.nofound') }}">No Service Found</a></li>
					<li><a href="{{ route('admin.request.rejected') }}">Rejected Request</a></li>
					<li><a href="{{ route('admin.request.schedule') }}">Scheduled Request</a></li>
					<li><a href="{{ route('admin.passenger.cancel.report') }}">Cancel By Passenger</a></li>
					<li><a href="{{ route('admin.driver.cancel.report') }}">Cancel By Driver</a></li>
					<li><a href="{{ route('admin.earning.report') }}">Admin Earning</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="#" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-car"></i></span>
					<span class="s-text">Vehicle</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.vehicle.index') }}">List Vehicles</a></li>
					<li><a href="{{ route('admin.vehicle.create') }}">Add New Vehicle</a></li>
				</ul>
			</li>
			
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-view-grid"></i></span>
					<span class="s-text">Service Types</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.service.index') }}">List Service Types</a></li>
					<li><a href="{{ route('admin.service.create') }}">Add New Service Type</a></li>
				</ul>
			</li>
			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">Documents</span>
				</a>
				<ul>
					<li><a href="{{ route('admin.document.index') }}">List Documents</a></li>
					<li><a href="{{ route('admin.document.create') }}">Add New Document</a></li>
				</ul>
			</li>

			<li class="with-sub">
				<a href="javascript:void(0)" class="waves-effect  waves-light">
					<span class="s-caret"><i class="fa fa-angle-down"></i></span>
					<span class="s-icon"><i class="ti-layout-tab"></i></span>
					<span class="s-text">Report an issue</span>
				</a>
				<ul>
					<li class="with-sub">
						<a href="javascript:void(0)" class="waves-effect  waves-light">
							{{-- <span class="s-icon"><i class="ti-layout-tab"></i></span> --}}
							<span class="s-text">Create Subject ?</span>
							<span class="s-caret" style="float: right;"><i class="fa fa-angle-down"></i></span>
						</a>
						<ul>
							<li><a href="{{route('admin.reportIssue.customer.subject.list')}}">Passenger</a></li>
							<li><a href="{{route('admin.reportIssue.driver.subject.list')}}">Driver</a></li>
						</ul>
					</li>
					<li class="with-sub">
						<a href="javascript:void(0)" class="waves-effect  waves-light">
							{{-- <span class="s-icon"><i class="ti-layout-tab"></i></span> --}}
							<span class="s-text">View Report an issue</span>
							<span class="s-caret" style="float: right;"><i class="fa fa-angle-down"></i></span>
						</a>
						<ul>
							<li><a href="{{route('admin.reportIssue.customer.list')}}">By Passenger</a></li>
							<li><a href="{{route('admin.reportIssue.driver.list')}}">By Driver</a></li>
						</ul>
					</li>
				</ul>
			</li>
			
			<li class="menu-title">Settings</li>
			<li>
				<a href="{{ route('admin.setting.index') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="ti-settings"></i></span>
					<span class="s-text">Site Settings</span>
				</a>
			</li>

			<li class="menu-title">Others</li>
			<li>
				<a href="{{ route('admin.privacy.policy') }}" class="waves-effect waves-light">
					<span class="s-icon"><i class="fa fa-file-text" aria-hidden="true"></i></span>
					<span class="s-text">CMS Management</span>
				</a>
			</li>
			<li class="menu-title">Account</li>
			<li>
				<a href="{{ route('admin.profile') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="fa fa-wrench" aria-hidden="true"></i></span>
					<span class="s-text">Account Settings</span>
				</a>
			</li>
			<li>
				<a href="{{ route('admin.password') }}" class="waves-effect  waves-light">
					<span class="s-icon"><i class="fa fa-key" aria-hidden="true"></i></span>
					<span class="s-text">Change Password</span>
				</a>
			</li>
			<li class="compact-hide">
				<a href="{{ url('/admin/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
					<span class="s-icon"><i class="fa fa-sign-out" aria-hidden="true"></i></span>
					<span class="s-text">Logout</span>
                </a>

                <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
			</li>
			
		</ul>
	</div>
</div>
