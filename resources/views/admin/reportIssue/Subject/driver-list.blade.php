@extends('admin.layout.base')

@section('title', 'Driver\'s Subject')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Driver's Subject
            </h5>
            <form class="form-horizontal" action="{{route('admin.reportIssue.driver.subject.add')}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <div class="form-group row">
                    <label for="subject" class="col-xs-2 col-form-label">Subject Name</label>
                    <div class="col-xs-12">
                        <input class="form-control" type="text" value="{{ old('subject') }}" name="subject" required id="subject" placeholder="Subject Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary">Add Subject</button>
                    </div>
                </div>
            </form>
            <br>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Subject</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ReportSubject as $index => $reportSubject)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $reportSubject->subject }}</td>
                        <td>{{date('Y-m-d h:i A',strtotime($reportSubject->created_at))}}</td>
                        <td>
                            <a href="{{ route('admin.reportIssue.driver.subject.edit', $reportSubject->report_subject_id) }}" class="btn btn-info"> Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Subject</th>
                        <th>Created On</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection