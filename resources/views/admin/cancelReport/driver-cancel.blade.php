@extends('admin.layout.base')

@section('title', 'Driver Cancel Report')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Driver Cancel Report</h5>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>Driver Name</th>
                        <th>Booking Number</th>
                        <th>Booking Date</th>
                        <th>Payment Status</th>
                        <th>Cancel Charge</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($driverReports as $index => $driverReport)
                    <tr>
                        <td>{{ $driverReport->first_name }} {{ $driverReport->last_name }}</td>
                        <td>{{ $driverReport->request_no }}</td>
                        <td>{{date('Y-m-d h:i A',strtotime($driverReport->created_at)) }}</td>
                        <td>{{ $driverReport->status }}</td>
                        <td>{{ $driverReport->currency }} {{ $driverReport->cost }}</td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.request.details', [$driverReport->request_id,'all']) }}" class="btn btn-info">View</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Driver Name</th>
                        <th>Booking Number</th>
                        <th>Booking Date</th>
                        <th>Payment Status</th>
                        <th>Cancel Charge</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection