@extends('admin.layout.base')

@section('title', 'No Service Found Request')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">No Service Found Request</h5>
                <table class="table table-striped table-bordered dataTable" id="table-no-request">
                    <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booking Number</th>
                            <th>Payment Method</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>Customer Name</th>
                            <th>Booking Number</th>
                            <th>Payment Method</th>
                            <th>Booking Status</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#table-no-request').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url":"{{route('admin.ajax-noservice-found')}}",
                "dataType":"json",
                "type":"POST",
                "data":{ "_token":"<?= csrf_token() ?>" },
            },
            "columns":[
                {"data":"customer_name"},
                {"data":"booking_number"},
                {"data":"payment_method"},
                {"data":"booking_status"},
                {"data":"action","searchable":false,"orderable":false}
            ],
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5'
            ],
        });
    });
</script>
@endsection