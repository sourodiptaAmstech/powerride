@extends('admin.layout.base')

@section('title', 'Service Type')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Service Type
            </h5>
            <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>Service Name</th>
                        <th>Registration No.</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Model Year</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{$ServiceType['serviceName']}}</td>
                        <td>{{$ServiceType['registration_no']}}</td>
                        <td>{{$ServiceType['make']}}</td>
                        <td>{{$ServiceType['model']}}</td>
                        <td>{{$ServiceType['model_year']}}</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Service Name</th>
                        <th>Registration No.</th>
                        <th>Make</th>
                        <th>Model</th>
                        <th>Model Year</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
