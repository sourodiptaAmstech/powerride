@extends('admin.layout.base')

@section('title', 'Documents ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Documents</h5>
                <a href="{{ route('admin.document.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Document</a>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Document Name</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($documents as $index => $document)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$document->name}}</td>
                            <td>{{$document->type}}</td>
                            <td style="line-height: 34px;">
                            @if ($document->status == 1)
                                <a class="documentStatus btn btn-success btn-xs" data-id="{{$document->id}}" data-value="Inactivate" href="javascript:viod(0)"
                                     data-toggle="tooltip" title="Click to Inactive">Active</a>
                                @else
                                <a class="documentStatus btn btn-danger btn-xs" data-id="{{$document->id}}" data-value="Activate" href="javascript:viod(0)"
                                     data-toggle="tooltip" title="Click to Active">Inactive</a>
                            @endif
                            <a href="{{ route('admin.document.edit', $document->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i> Edit</a>
                            <form class="doc-delete" action="{{ route('admin.document.destroy', $document->id) }}" method="POST">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="DELETE">
                                <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                            </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Document Name</th>
                            <th>Type</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.documentStatus', function(e){
            e.preventDefault();
            var id = $(this).data("id");
            var value = $(this).data("value");
            bootbox.confirm('Are you sure to '+value+' document?',function(result){
                if (result) {
                $.ajax({
                    url: "document/active/"+id,
                    type: 'GET',
                    data: {
                            "id": id
                        },
                    success: function (response){
                        location.reload();
                    },
                    error: function (response){
                        $('.msg_err').hide();
                        $('.msg_suc').hide();
                        $('.inactive_doc').show();
                        $('.inactive_msg').html(response.responseJSON.message);
                    }
                });
              }
            });
        });

        $(document).on('click','.doc-delete',function (e) {
            e.preventDefault();
            var form=$(this);
            bootbox.confirm('Do you really want to delete?', function (res) {
            if (res){
               form.submit();
            }
            });
        });
    });
</script>

@endsection