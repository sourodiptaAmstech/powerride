@extends('admin.layout.base')

@section('title', $leaseType)

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">{{$leaseType}}</h5>
                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Service Name</th>
                            <th>Address</th>
                            @if($leaseType=='Hourly Lease')
                            <th>Date of Journey</th>
                            <th>From Time</th>
                            <th>To Time</th>
                            <th>No. of Passenger</th>
                            @endif
                            @if($leaseType=='Daily Lease')
                            <th>Date of Journey</th>
                            <th>No. of Passenger</th>
                            @endif
                            @if($leaseType=='Long-Term Lease')
                            <th>From Date</th>
                            <th>To Date</th>
                            @endif
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($leases as $index => $lease)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$lease->first_name}} {{$lease->last_name}}</td>
                            <td>{{$lease->service_name}}</td>
                            <td>{{$lease->address}}</td>
                            @if($leaseType=='Hourly Lease')
                            <th>{{date('d M, Y',strtotime($lease->dateOfJourny))}}</th>
                            <td>{{date('h:i A',strtotime($lease->from_datetime))}}</td>
                            <td>{{date('h:i A',strtotime($lease->to_datetime))}}</td>
                            <td>{{$lease->no_passenger}}</td>
                            @endif
                            @if($leaseType=='Daily Lease')
                            <th>{{date('d M, Y',strtotime($lease->dateOfJourny))}}</th>
                             <td>{{$lease->no_passenger}}</td>
                            @endif
                            @if($leaseType=='Long-Term Lease')
                            <td>{{date('d M, Y',strtotime($lease->from_datetime))}}</td>
                            <td>{{date('d M, Y',strtotime($lease->to_datetime))}}</td>
                            @endif
                            
                            <td style="line-height: 33px;">
                                <a href="{{ route('admin.lease.view', [$lease->leasing_id,$lease->lease_type]) }}" class="btn btn-info">View</a>
                                @php
                                    $arrayName = array('lease'=>'lease','user'=>$lease->user_id);
                                    $encodeData = json_encode($arrayName);
                                    $param = encrypt($encodeData);
                                @endphp
                                <a href="{{ route('admin.passenger.chat.support') }}?param={{$param}}" class="btn btn-info"><i class="far fa-comment-dots"> Chat</i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Customer Name</th>
                            <th>Service Name</th>
                            <th>Address</th>
                            @if($leaseType=='Hourly Lease')
                            <th>Date of Journey</th>
                            <th>From Time</th>
                            <th>To Time</th>
                            <th>No. of Passenger</th>
                            @endif
                            @if($leaseType=='Daily Lease')
                            <th>Date of Journey</th>
                            <th>No. of Passenger</th>
                            @endif
                            @if($leaseType=='Long-Term Lease')
                            <th>From Date</th>
                            <th>To Date</th>
                            @endif
                    
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
