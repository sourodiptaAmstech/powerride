@extends('admin.layout.base')

@section('title', $leaseType)

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            @if($param=='daily')
            <a href="{{ route('admin.lease.daily') }}" style="margin-left: 1em;" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            @endif
            @if($param=='hourly')
            <a href="{{ route('admin.lease.hourly') }}" style="margin-left: 1em;" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            @endif
            @if($param=='longTime')
            <a href="{{ route('admin.lease.longTime') }}" style="margin-left: 1em;" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>
            @endif
            <h4 style="margin-bottom: 1em;">{{$leaseType}}</h4>

            <div class="row">
                <div class="col-md-8">
                    <h4 class="text-info">Customer Details:</h4>
                    <dl class="row">
                        <dt class="col-sm-4">Customer Name :</dt>
                        <dd class="col-sm-8">
                           {{$lease->first_name}} {{$lease->last_name}}
                        </dd>

                        <dt class="col-sm-4">Email ID :</dt>
                        <dd class="col-sm-8">
                            {{$lease->email_id}}
                        </dd>

                        <dt class="col-sm-4">Contact No. :</dt>
                        <dd class="col-sm-8">
                            {{$lease->isd_code}}-{{$lease->mobile_no}}
                        </dd>
                    </dl>
                    <h4 class="text-info">Lease Details:</h4>
                    <dl class="row">

                        @if($param=='hourly')
                        <dt class="col-sm-4">Date of Journey :</dt>
                        <dd class="col-sm-8">
                          {{$lease->dateOfJourny}}
                        </dd>
                        <dt class="col-sm-4">Time :</dt>
                        <dd class="col-sm-8">
                          {{date('h:i A',strtotime($lease->from_datetime))}} to {{date('h:i A',strtotime($lease->to_datetime))}}
                        </dd>
                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">
                            {{$lease->address}}
                        </dd>
                        <dt class="col-sm-4">No of Passenger :</dt>
                        <dd class="col-sm-8">
                            {{$lease->no_passenger}}
                        </dd>
                         @endif

                        @if($param=='daily')
                        <dt class="col-sm-4">Date of Journey :</dt>
                        <dd class="col-sm-8">
                          {{$lease->dateOfJourny}}
                        </dd>
                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">
                            {{$lease->address}}
                        </dd>
                        <dt class="col-sm-4">No of Passenger :</dt>
                        <dd class="col-sm-8">
                            {{$lease->no_passenger}}
                        </dd>
                        @endif

                        @if($param=='longTime')
                        <dt class="col-sm-4">Date :</dt>
                        <dd class="col-sm-8">
                          {{date('Y-m-d',strtotime($lease->from_datetime))}} to {{date('Y-m-d',strtotime($lease->to_datetime))}}
                        </dd>
                        <dt class="col-sm-4">Pickup Address :</dt>
                        <dd class="col-sm-8">
                            {{$lease->address}}
                        </dd>
                        @endif

                        <dt class="col-sm-4">Service Type :</dt>
                        <dd class="col-sm-8">
                            {{$lease->service_name}}
                        </dd>
                        @if($lease->services_type_id==4)
                        <dt class="col-sm-4">V/E Details :</dt>
                        <dd class="col-sm-8">
                            {{$lease->vehicle_equipment}}
                        </dd>
                        @endif
                        <dt class="col-sm-4">Other Details :</dt>
                        <dd class="col-sm-8">
                            {{$lease->details}}
                        </dd>
                       
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection