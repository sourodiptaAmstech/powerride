@extends('admin.layout.auth')

<!-- Main Content -->
@section('content')
<div class="sign-form">
    <div class="row">
        <div class="col-md-4 offset-md-4 px-3">
            <div class="box b-a-0">

                <div class="p-2 text-xs-center">
                    <h5>Forgot Password</h5>
                </div>

                <h3 class="success-otp text-success text-xs-center font-weight-bold" style="display: none; padding-top: 70px; padding-bottom: 70px;">OTP has been varified!</h3>

                <div class="p-0 text-xs-center">
                    <h4>Enter OTP</h4>
                <form>
                    <input id="codeBox1" type="number" maxlength="1" onkeyup="onKeyUpEvent(1, event)" onfocus="onFocusEvent(1)"/>
                    <input id="codeBox2" type="number" maxlength="1" onkeyup="onKeyUpEvent(2, event)" onfocus="onFocusEvent(2)"/>
                    <input id="codeBox3" type="number" maxlength="1" onkeyup="onKeyUpEvent(3, event)" onfocus="onFocusEvent(3)"/>
                    <input id="codeBox4" type="number" maxlength="1" onkeyup="onKeyUpEvent(4, event)" onfocus="onFocusEvent(4)"/>
                </form>
                <span class="text-danger otp-error text-xs-center"></span>
                </div>
                <div class="p-2 text-xs-center">
                    Didn't receive OTP?<a href="{{ route('password.reset') }}" class="btn btn-default"><i class="fa fa-angle-left"></i> Back to Resend </a>
                </div>

               
            </div>
        </div>
    </div>
</div>
<style>
    input[type=number] {
        height: 50px;
        width: 40px;
        font-size: 25px;
        text-align: center;
    }
    input[type=number]::-webkit-inner-spin-button,
    input[type=number]::-webkit-outer-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
</style>
@endsection
@section('scripts')
<script>
    function getCodeBoxElement(index) {
        return document.getElementById('codeBox' + index);
    }

  function onKeyUpEvent(index, event) {
    const eventCode = event.which || event.keyCode;
    if (getCodeBoxElement(index).value.length === 1) {
      if (index !== 4) {
        getCodeBoxElement(index+ 1).focus();
      } else {
        getCodeBoxElement(index).blur();

        var code1 = $('#codeBox1').val();
        var code2 = $('#codeBox2').val();
        var code3 = $('#codeBox3').val();
        var code4 = $('#codeBox4').val();

        $.ajax({
            url: "{{ route('password.otpvarify') }}",
            method: 'GET',
            data: { code1: code1, code2: code2, code3: code3, code4: code4 },
            success: function(response){
                if (response.success) {
                    $('#codeBox1').css({'display':'none'});
                    $('#codeBox2').css({'display':'none'});
                    $('#codeBox3').css({'display':'none'});
                    $('#codeBox4').css({'display':'none'});
                    $('.p-2').css({'display':'none'});
                    $('.p-0').css({'display':'none'});
                    $('.success-otp').css({'display':'block'}).delay(1000).fadeOut(1000, function(){
                            window.location.href = '{{ route("password.edit") }}';
                        });
                }
            },
            error: function(response){
                if (response.status == 422){
                  $('.otp-error').html(response.responseJSON.message);
                }
            }
        });

      }
    }
    if (eventCode === 8 && index !== 1) {
      getCodeBoxElement(index - 1).focus();
    }
  }

  function onFocusEvent(index) {
    for (item = 1; item < index; item++) {
      const currentElement = getCodeBoxElement(item);
      if (!currentElement.value) {
          currentElement.focus();
          break;
      }
    }
  }
</script>

@endsection