<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/login', 'Admin\Auth\LoginController@showLoginForm')->name('/login');
Route::get('/', 'Admin\Auth\LoginController@showLoginForm')->name('/');
Route::post('/loggedin', 'Admin\Auth\LoginController@login');
Route::get('/password/reset', 'Admin\Auth\ForgotPasswordController@otpRequestForm')->name('password.reset');
Route::post('password/email', 'Admin\Auth\ForgotPasswordController@sendOTP')->name('password.email');
Route::get('password/otp', 'Admin\Auth\ForgotPasswordController@otpForm')->name('password.otp');
Route::get('password/otpvarify', 'Admin\Auth\ForgotPasswordController@otpVarification')->name('password.otpvarify');
Route::get('password/edit', 'Admin\Auth\ForgotPasswordController@passworUpdateForm')->name('password.edit');
Route::post('password/update', 'Admin\Auth\ForgotPasswordController@updatePassword')->name('password.update');
Route::get('email/verification/{param}', 'Api\Payment\PaymentController@verifyEmail')->name("email.verification");

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'auth'], function () {

	Route::post('logout', 'Auth\LoginController@logout')->name('logout');

	Route::resource('dashboard', 'Dashboard\DashboardController');
	Route::get('dashboard/details/{id}/{param}', 'Dashboard\DashboardController@dashboardRequestDetails')->name('dashboard.details');

	Route::get('password', 'AdminController@password')->name('password');
	Route::post('password/update', 'AdminController@changePassword')->name('password.update');

	Route::get('profile', 'AdminController@profile')->name('profile');
	Route::post('profile/update', 'AdminController@profileUpdate')->name('profile.update');

	Route::resource('setting', 'Setting\SettingController');
	Route::get('privacy/policy', 'Setting\SettingController@privacyPolicy')->name('privacy.policy');
	Route::put('privacy-policy/update/{id}', 'Setting\SettingController@privacyPolicyUpdate')->name('privacy-policy.update');
	Route::put('settings/femalefriendly', 'Setting\SettingController@femaleFriendlyUpdate')->name('settings.femalefriendly');
	Route::get('get/femalefriendly', 'Setting\SettingController@getFemaleFriendly')->name('get.femalefriendly');

	Route::resource('document', 'Document\DocumentController');
	Route::get('document/active/{id}', 'Document\DocumentController@changeStatus')->name('document.active');

//lease...
	Route::get('lease/hourly', 'Lease\LeaseController@hourlyLease')->name('lease.hourly');
	Route::get('lease/daily', 'Lease\LeaseController@dailyLease')->name('lease.daily');
	Route::get('lease/longTime', 'Lease\LeaseController@longTimeLease')->name('lease.longTime');
	Route::get('lease/view/{id}/{param}', 'Lease\LeaseController@showLeaseDetails')->name('lease.view');

	Route::resource('service', 'Service\ServiceTypeController');

	Route::resource('vehicle', 'Vehicle\VehicleController');
	Route::post('vehicles','Vehicle\VehicleController@getVehicles')->name('dataProcessing');

	Route::resource('passenger', 'Passenger\PassengerController');
	Route::post('ajax-passenger','Passenger\PassengerController@ajaxPassenger')->name('ajax-passenger');
	
	Route::get('get/vehicle', 'Driver\DriverController@getVehicle')->name('get.vehicle');

	Route::resource('driver', 'Driver\DriverController');
	Route::post('ajax-driver','Driver\DriverController@ajaxDriver')->name('ajax-driver');

	Route::get('drivers/document/{id}', 'Driver\DriverController@listDriverDocument')->name('drivers.document');
	Route::get('drivers/service-type/{id}', 'Driver\DriverController@listDriverServiceType')->name('drivers.service-type');
	Route::get('driver/document/verification/{id}', 'Driver\DriverController@driverDocumentVerification')->name('driver.document.verification');
	Route::get('driver/activation/{id}', 'Driver\DriverController@driverActivationProcess')->name('driver.activation');
	  //review rating
	Route::get('drivers/review-rating/{id}', 'Driver\DriverController@driverRatingReview')->name('drivers.review-rating');
	Route::get('passenger/review-rating/{id}', 'Passenger\PassengerController@passengerRatingReview')->name('passenger.review-rating');

	Route::put('drivers/femalefriendly', 'Driver\DriverController@isFemaleFriendly')->name('drivers.femalefriendly');

	//report
	Route::get('request/all', 'DriveReport\DriveReportController@allRequest')->name('request.all');
	Route::post('ajax-all-request', 'DriveReport\DriveReportController@ajaxAllRequest')->name('ajax-all-request');
	Route::get('request/nofound', 'DriveReport\DriveReportController@noServiceFoundRequest')->name('request.nofound');
	Route::post('ajax-noservice-found', 'DriveReport\DriveReportController@ajaxNoServiceFoundRequest')->name('ajax-noservice-found');

	Route::get('request/rejected', 'DriveReport\DriveReportController@rejectedRequest')->name('request.rejected');
	Route::post('ajax-rejected-request', 'DriveReport\DriveReportController@ajaxRejectedRequest')->name('ajax-rejected-request');

	Route::get('request/schedule', 'DriveReport\DriveReportController@scheduleRequest')->name('request.schedule');
	Route::post('ajax-schedule-request', 'DriveReport\DriveReportController@ajaxScheduleRequest')->name('ajax-schedule-request');

	Route::get('request/details/{id}/{param}', 'DriveReport\DriveReportController@requestDetails')->name('request.details');

	//chat support
	Route::get('passenger/chat/support', 'Chat\ChatSupportController@passengerChatSupport')->name('passenger.chat.support');

	Route::post('chat/message/support', 'Chat\ChatSupportController@sendChatForSupport')->name('chat.message.support');

	Route::post('chat/message/support/get', 'Chat\ChatSupportController@getChatForSupport')->name('chat.message.support.get');

	Route::get('driver/chat/support', 'Chat\ChatSupportController@driverChatSupport')->name('driver.chat.support');

	Route::get('driver/get/byid', 'Chat\ChatSupportController@getDriver')->name('driver.get.byid');
	Route::get('passenger/get/byid', 'Chat\ChatSupportController@getPassenger')->name('passenger.get.byid');

	//make model year
	Route::get('get/admin/make', 'Driver\DriverController@getAdminMake')->name("get.admin.make");
	Route::post('get/admin/model', 'Driver\DriverController@getAdminModel')->name("get.admin.model");
	Route::post('get/admin/year', 'Driver\DriverController@getAdminYear')->name("get.admin.year");

	 //passenger trip
	Route::get('passenger/trip/history/{id}', 'Passenger\TripController@tripIndex')->name("passenger.trip.history");
	Route::post('ajax-passenger-trips', 'Passenger\TripController@ajaxPassengerTrips')->name('ajax-passenger-trips');
	Route::get('passenger/trip/history/details/{id}/{param}', 'Passenger\TripController@tripDetails')->name('passenger.trip.history.details');

	 //Driver trip
	Route::get('driver/trip/history/{id}', 'Driver\DriverTripController@tripIndex')->name("driver.trip.history");
	Route::post('ajax-driver-trips', 'Driver\DriverTripController@ajaxDriverTrips')->name('ajax-driver-trips');
	Route::get('driver/trip/history/details/{id}/{param}', 'Driver\DriverTripController@tripDetails')->name('driver.trip.history.details');
	 //transaction
	Route::get('driver/transaction/{id}', 'Transaction\TransactionController@index')->name('driver.transaction');
	Route::post('driver/transaction', 'Transaction\TransactionController@transactionPayout')->name('driver.transaction');

	Route::get('passenger/transaction/{id}', 'Transaction\TransactionController@passengerTransaction')->name('passenger.transaction');
	Route::get('passenger/transac/list', 'Transaction\TransactionController@passengerTransactionList')->name('passenger.transac.list');

	Route::get('earning/report', 'Transaction\TransactionController@adminEarning')->name('earning.report');

	Route::resource('promocode', 'Promocode\PromocodeController');

	Route::get('passenger/cancel/report', 'CancelCharge\CancelChargeController@passengerCancelReport')->name('passenger.cancel.report');
	Route::get('driver/cancel/report', 'CancelCharge\CancelChargeController@driverCancelReport')->name('driver.cancel.report');

	 //report issue
	 Route::get('reportIssue/driver/subject/list', 'ReportIssue\ReportController@driverSubjectList')->name('reportIssue.driver.subject.list');
	 Route::post('reportIssue/driver/subject/add', 'ReportIssue\ReportController@driverSubjectAdd')->name('reportIssue.driver.subject.add');
	 Route::get('reportIssue/driver/subject/edit/{id}', 'ReportIssue\ReportController@driverSubjectEdit')->name('reportIssue.driver.subject.edit');
	 Route::put('reportIssue/driver/subject/update/{id}', 'ReportIssue\ReportController@driverSubjectUpdate')->name('reportIssue.driver.subject.update');
 
	 Route::get('reportIssue/customer/subject/list', 'ReportIssue\ReportController@customerSubjectList')->name('reportIssue.customer.subject.list');
	 Route::post('reportIssue/customer/subject/add', 'ReportIssue\ReportController@customerSubjectAdd')->name('reportIssue.customer.subject.add');
	 Route::get('reportIssue/customer/subject/edit/{id}', 'ReportIssue\ReportController@customerSubjectEdit')->name('reportIssue.customer.subject.edit');
	 Route::put('reportIssue/customer/subject/update/{id}', 'ReportIssue\ReportController@customerSubjectUpdate')->name('reportIssue.customer.subject.update');
 
	 Route::get('reportIssue/driver/list', 'ReportIssue\ReportController@driverReportIssueList')->name('reportIssue.driver.list');
	 Route::get('reportIssue/customer/list', 'ReportIssue\ReportController@customerReportIssueList')->name('reportIssue.customer.list');
	 Route::get('reportIssue/details/{id}/{param}', 'ReportIssue\ReportController@reportIssueDetails')->name('reportIssue.details');

	// Route::get('driver/carimages/{id}', 'Driver\DriverController@driverCarImages')->name('driver.carimages');
	// Route::get('driver/transaction/{id}', 'Driver\DriverController@driverTransaction')->name('driver.transaction');
	// Route::post('driver/document-reason/{id}', 'Driver\DriverController@documentVerificationReason')->name('driver.document-reason');

	// Route::resource('credit', 'CreditPackage\CreditPackageController');
	// Route::resource('email-control', 'Email\EmailController');

	// //Driver Document verification.........

	// Route::get('driver/check-document/{id}', 'Driver\DocumentVerificationController@allDocumentCheck')->name('driver.check-document');
	// Route::get('driver/rating-review/{id}', 'Driver\DriverRatingController@driverRatingReview')->name('driver.rating-review');
	// Route::get('driver/runcron/{id}', 'Driver\DocumentVerificationController@cronjobToMailDriver')->name('driver.runcron');
	// //.....................................

	// Route::get('driver/rating-list/{id}/{last_id}/{first_id}', 'Driver\DriverRatingController@ratingList')->name('driver.rating-list');
	// Route::post('driver/reason/{id}', 'Driver\DriverController@driverInactiveBannedReason')->name('driver.reason');
	// Route::get('driver/rating-search/{id}', 'Driver\DriverRatingController@searchRating')->name('driver.rating-search');

	// Route::post('driver/rating-alert/{id}', 'Driver\DriverRatingController@driverRatingAlert')->name('driver.rating-alert');
	// //payment

	// Route::get('payment/index', 'Payment\PaymentController@paymentList')->name('payment.index');

	// Route::get('package/sales/report', 'PackageSales\PackageSalesReportController@packageSalesReport')->name('package.sales.report');
	// Route::get('package/sales/report/search', 'PackageSales\PackageSalesReportController@searchPackageSalesReport')->name('package.sales.report.search');
	// Route::get('package/sales/report/details/{id}', 'PackageSales\PackageSalesReportController@packageSalesReportDetails')->name('package.sales.report.details');
	// Route::get('package/sales/report/detail/{id}/{from_date}/{to_date}', 'PackageSales\PackageSalesReportController@packageSalesReportDetailsBySearch')->name('package.sales.report.detail');

	// //ride history
	// Route::get('statement/{type}', 'Statement\StatementController@statement')->name('statement');
	// 	//not use
	// 	Route::get('statement/today', 'Statement\StatementController@statement_today')->name('statement.today');
	// 	Route::get('statement/weekly', 'Statement\StatementController@statement_weekly')->name('statement.weekly');
	// 	Route::get('statement/monthly', 'Statement\StatementController@statement_monthly')->name('statement.monthly');
	// 	Route::get('statement/yearly', 'Statement\StatementController@statement_yearly')->name('statement.yearly');
	// 	//not use
	// Route::post('ajax-statement','Statement\StatementController@ajaxStatement')->name('ajax-statement');
	// //............

	// Route::get('statement/details/{id}/{param}', 'Statement\StatementController@rideDetails')->name('statement.details');

	// Route::get('statement/driver/index', 'Statement\StatementController@driverStatementIndex')->name('statement.driver.index');
	// Route::post('ajax-driver-statement','Statement\StatementController@ajaxDriverStatement')->name('ajax-driver-statement');

	// Route::get('statement/driver/details/{id}', 'Statement\StatementController@driverStatementDetails')->name('statement.driver.details');
	// Route::get('statement/driver/detailsbybid/{id}', 'Statement\StatementController@driverStatementDetailsByBid')->name('statement.driver.detailsbybid');
	
	// Route::get('report-issue/bydriver', 'ReportIssue\ReportIssueController@reportedByDriver')->name('report-issue.bydriver');
	// Route::get('report-issue/bypassenger', 'ReportIssue\ReportIssueController@reportedByPassenger')->name('report-issue.bypassenger');
	// Route::get('report-issue/details/{id}/{param}', 'ReportIssue\ReportIssueController@reportIssueDetails')->name('report-issue.details');

	

});